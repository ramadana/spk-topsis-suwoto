<style>
#box_info{
width:100%;
height:90px;
margin-bottom:55px;
}
#gambar{
float:left;
width:150px;
height:120px;
border-bottom:solid 1px #999999;
margin-right:5px;
}
#keterangan{
float:left;
width:790px;
height:120px;
border-bottom:solid 1px #999999;

}
</style>
<?php  for ($a=1; $a<=4; $a++){?>
<div id="box_info">
	<div id="gambar">
			<?php  
			if ($a==1) { echo "<img src='library/images/pdi1.jpg' style='width:150px'>";}
			elseif ($a==2) { echo "<img src='library/images/pdi2.jpg' style='width:150px'>";}
			elseif ($a==3) { echo "<img src='library/images/pdi3.jpg' style='width:150px'>";}
			elseif ($a==4) { echo "<img src='library/images/pdi4.jpg' style='width:150px'>";}

			?>
			
			
	</div>
	<div id="keterangan">
		
		<?php  
			if ($a==1)
			{
				echo "<h5>SITU BAGENDIT</h5>";
				echo "<p1 align='justify'>";
				echo "Padi varietas Situ Bagendit adalah salah satu varietas padi gogo, tetapi mampu tumbuh baik pada lingkungan lahan sawah. Tanaman ini mempunyai tinggi antara 99 - 105 cm, dengan umur tanaman 110 - 120 hari setelah sebar (HSS). Varietas Situ Bagendit memiliki bentuk biji ramping, warna gabah kuning bersih, dengan bobot 1000 butir adalah 27,5 gram. Varietas ini mempunyai anakan produktif 12 - 13 batang/rumpun.";
	  			echo "</p1>";
			}
			elseif ($a==2) 
			{
				echo "<h5>Ciherang</h5>";
				echo "<p1 align='justify'>";
				echo "Merupakan hasil persilangan IR 64 terhadap beberapa galur IR lainnya ini, tampil dengan perkasa mengalah­kan dominasi IR 64 selama kurun waktu 6 tahun dan eksis di Indonesia selama 10 tahun terakhir. Selain me­rupakan nama jaminan kualitas be­ras, padi Ciherang dikenal tahan ter­hadap hama dan penyakit terutama hama Wereng Coklat biotipe 2 dan 3 serta penyakit Hawar Daun Bakteri strain III dan IV.";
	  			echo "</p1>";
			}
			elseif ($a==3) 
			{
				echo "<h5>IR 64</h5>";
				echo "<p1 align='justify'>";
				echo "Padi IR 64 merupakan salah satu varietas unggul padi sawah yang dilepas pemerintah mulai tahun 1986. Sampai saat ini masih disukai petani, karena umur tanam lebih pendek, nasinya pulen, dan mudah dijual karena harga terjangkau oleh masyarakat. Untuk memperoleh hasil padi IR 64 yang tinggi harus menggunakan benih bermutu dengan varietas unggul, yaitu benih padi IR 64 yang bersertifikat.";
	  			echo "</p1>";
			}
			else 
			{
				echo "<h5>Cibogo</h5>";
				echo "<p1 align='justify'>";
				echo "Pemerintah melalui Kementrian Pertanian melepas beberapa varietas padi, salah satunya antara lain : Varietas Cibogo.dengan ciri-ciri antara lain sbb : Potensi dan Rerata : 8.1 ton/ha GKG – 7 ton/ha GKG Karakteristik Khusus, Asal persilangan : IR487B-752/IR19661-131-3-1//IR19661-131-3-1///IR64////IR64 Golongan : Cere, Umur tanaman : 115-125 hari,Tekstur nasi : Pulen ,Bobot 1000 butir : 27-30 gram, Potensi hasil : 8.1 ton/ha GKG.";
			    echo "</p1>";
			}
		?>
		




		
	</div>
	
</div>
<?php  }?>