<style>
#tabel_jawaban td{
padding:10px;

}
#tabel_jawaban thead tr td{
font-weight:bold;
background:#d3d3d3;
}
#tabel_jawaban tbody tr td{
background:#ffffff;
}
</style>

<script type="text/javascript">
	
	function print_button_click(){
		print();
	}

	$(document).ready(function(){

	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

});
</script>

</script>

<?php 
if (isset ($_SESSION['nama']))
$user=$_SESSION['nama'];
if (empty ($user)){
		echo "<script>alert('Masukkan nama terlebih dahulu');
		document.location.href='index.php?id=start_spk';</script>\n";	
}

$sql="select * from spk_hasil_penilaian where user='$user'";
$hasil=mysql_query($sql);

?>

<!-- Nama User -->
<h2>Nama User : <font color="#00AAAA"><?php echo $_SESSION['nama']; ?></font><button type="button" id="print" class="btn btn-default" style="padding: 2px 12px 2px 12px;float:right; background-color:#00AAAA;" onclick="return print_button_click();"><img src="resources/printer_white.png" alt="print" style="max-height: 24px; max-width: 24px;"></button></h2>

<hr>

<!-- Tabel Jawaban -->
<h4>Jawaban User</h4>
<div class="table-responsive">
<table border='1' id="tabel_jawaban" class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th style="text-align:center;">No</th>
			<th style="text-align:center;">Kategori</th>
			<th style="text-align:center;">Jawaban 1</th>
			<th style="text-align:center;">Jawaban 2</th>
			<th style="text-align:center;">Jawaban 3</th>
			<th style="text-align:center;">Jawaban 4</th>
		</tr>
	</thead>
<tbody>
<?php 
$no=1;
	while ($data=mysql_fetch_array($hasil)) { 
		$kat=$data['kategori'];
		$hasil_kat=mysql_query("select * from spk_kategori_benih where id='$kat'");
		$data_kat=mysql_fetch_array($hasil_kat)

				?>
					<tr>
						<td style="text-align:center;"><?php  echo $no; ?></td>
						<td ><?php  echo $data_kat['kategori']; ?></td>
						<td style="text-align:center;"><?php  echo $data['jawaban_k1']; ?></td>
						<td style="text-align:center;"><?php  echo $data['jawaban_k2']; ?></td>
						<td style="text-align:center;"><?php  echo $data['jawaban_k3']; ?></td>
						<td style="text-align:center;"><?php  echo $data['jawaban_k4']; ?></td>
						
					</tr>

				<?php 
				$no++;
			}
		?>
	</tbody>
</table>
<br>


<!-- tampilan matriks R -->
<h4>Hasil Matrix_r</h4>
<table border='1' id="tabel_jawaban" class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th style="text-align:center;">No</th>
			<th style="text-align:center;">r1</th>
			<th style="text-align:center;">r2</th>
			<th style="text-align:center;">r3</th>
			<th style="text-align:center;">r4</th>
		</tr>
	</thead>
<tbody>
	<?php 
		$no=1;
		$r_sql=mysql_query("select * from proses_matrix_r where user='$user'");
		while ($data=mysql_fetch_array($r_sql)) { 
	?>
			<tr>
				<td><?php  echo $no; ?></td>
				<td><?php  echo $data['r1']; ?></td>
				<td><?php  echo $data['r2']; ?></td>
				<td><?php  echo $data['r3']; ?></td>
				<td><?php  echo $data['r4']; ?></td>
			
			</tr>

	<?php 
		$no++;
	}
	?>
</tbody>
</table>
<br>

<!-- tampilan matriks y -->
<h4>Hasil Matrix_y</h4>
<table border='1' id="tabel_jawaban" class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th style="text-align:center;">No</th>
			<th style="text-align:center;">y1</th>
			<th style="text-align:center;">y2</th>
			<th style="text-align:center;">y3</th>
			<th style="text-align:center;">y4</th>
		</tr>
	</thead>
<tbody>
	<?php 
		$no=1;
		$r_sql=mysql_query("select * from proses_matrix_y where user='$user'");
		while ($data=mysql_fetch_array($r_sql)) { 
	?>
			<tr>
				<td><?php  echo $no; ?></td>
				<td><?php  echo $data['y1']; ?></td>
				<td><?php  echo $data['y2']; ?></td>
				<td><?php  echo $data['y3']; ?></td>
				<td><?php  echo $data['y4']; ?></td>
				
			</tr>

		<?php 
			$no++;
		}
		?>
	</tbody>
</table>
<br>

<!-- Solusi -->
<h4>Solusi</h4>
<table border='1' id="tabel_jawaban">
  <thead>
    <tr>
      <td>No</td>
      <td>a1</td>
      <td>a2</td>
      <td>a3</td>
      <td>a4</td>
      
      <td>Status Jarak</td>
    </tr>
  </thead>
  <tbody>
    <?php 
		$no=1;
		$r_sql=mysql_query("select * from proses_solusi where user='$user'");
		while ($data=mysql_fetch_array($r_sql)) { 
	?>
    <tr>
      <td><?php  echo $no; ?></td>
      <td><?php  echo $data['a1']; ?></td>
      <td><?php  echo $data['a2']; ?></td>
      <td><?php  echo $data['a3']; ?></td>
      <td><?php  echo $data['a4']; ?></td>
     
      <td><?php  echo $data['status_solusi']; ?></td>
    </tr>
    <?php 
		$no++;
	}
	?>
  </tbody>
</table>
<br>

<!-- Jarak -->
<h4>Jarak</h4>
<table border='1' id="tabel_jawaban" class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th style="text-align:center;">No</th>
			<th style="text-align:center;">d1</th>
			<th style="text-align:center;">d2</th>
			<th style="text-align:center;">d3</th>
			<th style="text-align:center;">d4</th>
			<th style="text-align:center;">Status Jarak</th>
		</tr>
	</thead>
<tbody>
	<?php 
		$no=1;
		$r_sql=mysql_query("select * from proses_jarak where user='$user'");
		while ($data=mysql_fetch_array($r_sql)) { 
	?>
			<tr>
				<td><?php  echo $no; ?></td>
				<td><?php  echo $data['d1']; ?></td>
				<td><?php  echo $data['d2']; ?></td>
				<td><?php  echo $data['d3']; ?></td>
				<td><?php  echo $data['d4']; ?></td>
				
				<td><?php  echo $data['status_jarak']; ?></td>
			</tr>

		<?php 
			$no++;
		}
		?>
	</tbody>
</table>
<br>

<!-- Hasil -->
<hr>
<h4>Hasil Sistem Berdasarkan Urutan data</h4>
<table border='1' id="tabel_jawaban">
	<thead>
	<tr>
		<td>No</td>
		<td>Jenis Benih</td>
		<td>Hasil Test</td>
		
	</tr>
	</thead>
<tbody>
	<?php 
		$no=1;
		$h_sql=mysql_query("select * from hasil_benih where user='$user'");
		$h_data_max = 0;
		$jenis_benih = array("SITU BAGENDIT", "CIHERANG", "IR 64", "CIBOGO");
		$cnt_max = 0;
		while ($h_data=mysql_fetch_array($h_sql)) { 
	?>
			<tr>
				<td><?php  echo $no; ?></td>
				<td>
					<?php  
						echo $jenis_benih[$no-1];	    
					?>
				</td>
				<td>
					<?php  
						echo $h_data['alt']; 
						if ($h_data['alt'] > $h_data_max) {
							$h_data_max = $h_data['alt'];
							$cnt_max = $no-1;
						}
					?>
				</td>
			</tr>

	<?php 
		$no++;
	}
?>
<table class="table table-bordered table-hover table-striped">
    <thead>
        <tr>
            <th style="text-align:center;">No</th>
            <th style="text-align:center;">Jenis Benih</th>
            <th style="text-align:center;">Hasil Test</th>
        </tr>
    </thead>
    <tbody>

    	<?php 
    		for ($itemCount=0; $itemCount<count($hasil_data); $itemCount++) {
    			$data = $hasil_data[$itemCount];

    			// Pewarna Baris | Success = Hijau | Danger = Merah
    			if ($itemCount == 0) {
    				
    				if ($hasil_data[0]['alt'] > $hasil_data[count($hasil_data)-1]['alt']) echo '<tr class="success">';
    				else echo '<tr class="danger">';
    			}
    			else if ($itemCount == count($hasil_data)-1) {
    				if ($hasil_data[0]['alt'] > $hasil_data[count($hasil_data)-1]['alt']) echo '<tr class="danger">';
    				else echo '<tr class="success">';
    			}
    			else echo "<tr>";

    			// Nomer Urut
				echo '<td align="center">';
					echo $itemCount+1; // No Urut Statis
					// echo $data['id_kategori']; // No Urut Dinamis
				echo '</td>';
    			
    			// Jenis Benih
				echo '<td style="text-align:center;">';
					echo $data['kategori'];
				echo '</td>';

				// Hasil Test
				echo '<td style="text-align:center;">';
					echo $data['alt'];
				echo '</td>';

				// Close the row
    			echo "</tr>";
    		}

    	?>
    </tbody>
</table>

</div>


<?php 
$result=mysql_query('select * from hasil_benih');
$data_result=mysql_fetch_array($result);

echo "<div>";
echo "<p><strong>Kesimpulan :</strong> Sebaiknya anda memilih benih padi <strong>{$hasil_data[0]['kategori']} ({$hasil_data[0]['alt']})</strong><strong>. </strong><br>Hasil tersebut adalah hasil perhitungan SPK dengan menggunakan metode TOPSIS dari inputan yang anda masukkan tadi. Semoga Bermanfaat. </p>";
echo "</div>";
?>

<hr>

<p id="back-top" style="float:right;"><a href="#">Back to Top</a></p>
