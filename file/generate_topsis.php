<?php 
function topsis($nama){
	//ambil data dari database
	$query=mysql_query("select * from spk_hasil_penilaian where user='$nama' order by id ASC");
	$i=1;
	$arrKategori = array();
	while($m=mysql_fetch_array($query)){
		$matrix1['k1'][$i]=$m['jawaban_k1'];
		$matrix1['k2'][$i]=$m['jawaban_k2'];
		$matrix1['k3'][$i]=$m['jawaban_k3'];
		$matrix1['k4'][$i]=$m['jawaban_k4'];
		$arrKategori[$i] = $m['kategori'];
		$i++;
	}
	
	//membuat matrix keputusan ternormalisasi (R)
	$j=1;
	foreach($matrix1 as $key=>$matrix){
		$x=sqrt(pow($matrix[1],2) + pow($matrix[2],2) + pow($matrix[3],2) + pow($matrix[4],2));
		$r[1][$j]=$matrix[1]/$x;
		$r[2][$j]=$matrix[2]/$x;
		$r[3][$j]=$matrix[3]/$x;
		$r[4][$j]=$matrix[4]/$x;
		$j++;
	}
	
	//simpan data R kedalam tabel matrix_r
	foreach($r as $key=>$mr){
		$q=mysql_query("insert into matrix_r values('','$nama','".$mr[1]."','".$mr[2]."','".$mr[3]."','".$mr[4]."')");
	}
	
	//memuat matrix keputusan terormalisasi terbobot (Y) dan simpan data di tabel matrix_y
	$matrix_r=mysql_query("select * from proses_matrix_r where user='$nama' order by id_matrix_r ASC");
	while($n_matrix_r=mysql_fetch_array($matrix_r)){
		$y1=$n_matrix_r['r1']*3;
		$y2=$n_matrix_r['r2']*2;
		$y3=$n_matrix_r['r3']*3;
		$y4=$n_matrix_r['r4']*3;
		$y=mysql_query("insert into matrix_y values('','$nama','$y1','$y2','$y3','$y4')");
	}
	
	//mencari solusi ideal positif dan negatif kemudian masukkan tabel solusi
	$solusi=mysql_fetch_array(mysql_query("select max(y1),max(y2),max(y3),max(y4),min(y1),min(y2),min(y3),min(y4) from proses_matrix_y where user='$nama'"));
	$max_a1=$solusi['max(y1)']	;
	$max_a2=$solusi['max(y2)']	;
	$max_a3=$solusi['max(y3)']	;
	$max_a4=$solusi['max(y4)']	;
	
	$min_a1=$solusi['min(y1)']	;
	$min_a2=$solusi['min(y2)']	;
	$min_a3=$solusi['min(y3)']	;
	$min_a4=$solusi['min(y4)']	;

	
	$in_solusi_plus=mysql_query("insert into solusi values('','$nama','$max_a1','$max_a2','$max_a3','$max_a4','+')");
	$in_solusi_min=mysql_query("insert into solusi values('','$nama','$min_a1','$min_a2','$min_a3','$min_a4','-')");
	
	//menghitung jarak alternatif sekaligus alternatif terakhir, simpan di tabel jarak dan alternatif
	$matrix_y=mysql_query("select * from proses_matrix_y where user='$nama' order by id_matrix_y ASC");
	while($n_matrix_y=mysql_fetch_array($matrix_y)){
		$plus1=pow(($n_matrix_y['y1']-$max_a1),2);
		$plus2=pow(($n_matrix_y['y2']-$max_a2),2);
		$plus3=pow(($n_matrix_y['y3']-$max_a3),2);
		$plus4=pow(($n_matrix_y['y4']-$max_a4),2);

		$total_plus=sqrt($plus1+$plus2+$plus3+$plus4);
		$jarak_plus[]=$total_plus;
		$min1=pow(($n_matrix_y['y1']-$min_a1),2);
		$min2=pow(($n_matrix_y['y2']-$min_a2),2);
		$min3=pow(($n_matrix_y['y3']-$min_a3),2);
		$min4=pow(($n_matrix_y['y4']-$min_a4),2);

		$total_min=sqrt($min1+$min2+$min3+$min4);
		$jarak_min[]=$total_min;
		$alternatif=$total_min/($total_plus+$total_min);
		$alt=mysql_query("insert into analisa_benih values('','$nama','$alternatif', $arrKategori[$i])");
		$i++;
	}
	
	$in_jarak_plus=mysql_query("insert into jarak values('','$nama','$jarak_plus[0]','$jarak_plus[1]','$jarak_plus[2]','$jarak_plus[3]','+')");
	$in_jarak_min=mysql_query("insert into jarak values('','$nama','$jarak_min[0]','$jarak_min[1]','$jarak_min[2]','$jarak_min[3]','-')");
	
}
?>
