<?php 
	$sql="select * from spk_data_pertanyaan order by kategori ASC";
	$hasil=mysql_query($sql);

	$update_status = $_GET["update_status"];
	if ($update_status == "success") echo "<script>alertify.success('Data Update Success !');</script>";
	else if ($update_status == "failed") {
		echo '<script>alertify.error("Error. Update pertanyaan gagal !")</script>';	
	}
?>

<!-- Load EZScrollJS -->
<script src="../library/js/ezscroll.js"></script> 
<script type="text/javascript">
	
	function tambah_kategori(){
		var tableRef = document.getElementById('table').getElementsByTagName('tbody')[0];

		// Insert a row in the table at the last row
		var newRow   = tableRef.insertRow(tableRef.rows.length);
		newRow.className = "form-group";

		// Insert a cell in the row at index 0
		// var newCell  = newRow.insertCell(0);
		var orderNumberCell = newRow.insertCell(0);
		var kategoriCell = newRow.insertCell(1);
		var opsi1Cell = newRow.insertCell(2);
		var opsi2Cell = newRow.insertCell(3);
		var opsi3Cell = newRow.insertCell(4);
		var opsi4Cell = newRow.insertCell(5);
		var updateCell = newRow.insertCell(6);
		var deleteCell = newRow.insertCell(7);

		// Order Number
		var orderNumber  = document.createTextNode('No');
		orderNumberCell.appendChild(orderNumber);

		// Kolom kategori
		var kolomKategori = document.createElement("input");
		kolomKategori.className = "form-control";
		kolomKategori.placeholder = "kategori";
		kategoriCell.appendChild(kolomKategori);

		// Opsi 1
		var opsi1 = document.createElement("input");
		opsi1.className = "form-control";
		opsi1.placeholder = "opsi 1";
		opsi1Cell.appendChild(opsi1);

		// Opsi 2
		var opsi2 = document.createElement("input");
		opsi2.className = "form-control";
		opsi2.placeholder = "opsi 2";
		opsi2Cell.appendChild(opsi2);

		// Opsi 3
		var opsi3 = document.createElement("input");
		opsi3.className = "form-control";
		opsi3.placeholder = "opsi 3";
		opsi3Cell.appendChild(opsi3);

		// Opsi 4
		var opsi4 = document.createElement("input");
		opsi4.className = "form-control";
		opsi4.placeholder = "opsi 4";
		opsi4Cell.appendChild(opsi4);

		// Submit
		var updateButton = document.createElement("button");
		updateButton.type = "button";
		updateButton.className = "btn btn-sm btn-success";
		var updateButtonText = document.createTextNode('+');
		updateButton.appendChild(updateButtonText);
		updateButton.onclick = function() {
			alert('Mohon maaf untuk sementara fungsi masih belum diaplikasikan');
		}
		updateCell.appendChild(updateButton);

		// Reset
		var resetButton = document.createElement("button");
		resetButton.type = "button";
		resetButton.className = "btn btn-sm btn-danger";
		var resetButtonText = document.createTextNode('-');
		resetButton.appendChild(resetButtonText);
		resetButton.onclick = function() {
			var i = newRow.rowIndex;
			document.getElementById("table").deleteRow(i);
			document.getElementById("addNotification").style.display = "none";
		}
		deleteCell.appendChild(resetButton);

		document.getElementById("addNotification").style.display = "";
		EPPZScrollTo.scrollVerticalToElementById("bottom_add_row_button", 100);
	}

</script>

<div>	
<h3>Daftar Pertanyaan SPK &nbsp;&nbsp;<button type="button" class="btn btn-sm btn-success" float="right" onclick="return tambah_kategori();">Tambah Kategori</button></h3>
	
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" border="0" id="table" class="table table-bordered table-hover table-striped sortable">
			<thead>
				<tr>
					<th><h3>No</h3></th>
					<th><h3>Kategori</h3></th>
					<th><h3>Opsi 1</h3></th>
					<th><h3>Opsi 2</h3></th>
					<th><h3>Opsi 3</h3></th>
					<th><h3>Opsi 4</h3></th>	
					<th colspan="2"><h3>Option</h3></th>
				</tr>
			</thead>
			<tbody>
			
			<?php  $no=1;  while ($data=mysql_fetch_array($hasil)) { 
				$id=$data['kategori'];
				$sql_kat=mysql_query("select kategori from spk_kategori_benih where id=$id");
				$data_kat=mysql_fetch_array($sql_kat);
			
			?>
				<tr>
					<td id="no-<?php echo $no; ?>"><?php  echo $no; ?></td>
					<td><?php  echo $data_kat['kategori']; ?></td>
					<td><?php  echo $data['k1']; ?></td>
					<td><?php  echo $data['k2']; ?></td>
					<td><?php  echo $data['k3']; ?></td>
					<td><?php  echo $data['k4']; ?></td>
					
					<td>
						<a href="index.php?<?php echo $id.'&'.$user.'&'.$ref.'&';?>&page=update_pertanyaan&index=<?php echo $data[0]; ?>">update</a>
					</td>
					<td>
						<a href="index.php?<?php echo $id.'&'.$user.'&'.$ref.'&';?>page=proses_pertanyaan&act=delete&index=<?php echo $data[0]; ?>">del</a>
					</td>
					
					
				</tr>
			<?php  $no++; } ?>	
			</tbody>
		</table>
		<div>
			<label id="addNotification" name="addNotification" style="float:left;display:none;"><font size="1" color="red" face="sans-serif"><i>Silahkan masukkan data-data untuk kriteria baru diatas</i></font></label>
			<button type="button" id="bottom_add_row_button" class="btn btn-sm btn-success" float="right" onclick="return tambah_kategori();" style="float:right;">Tambah Kategori</button>
		</div>
	</div>

</div>  