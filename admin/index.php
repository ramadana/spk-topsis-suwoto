<?php
include ("../library/koneksi.php");
session_start();
if (isset ($_SESSION['id']))
$license=$_SESSION['id'];
if (empty ($license)){
		echo "<script>document.location.href='../login.php?status=failed';</script>\n";
}else{

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SPK Benih Padi - Halaman Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Custom Alert Dialog -->
    <script src="../js/alertify.js"></script>
    <link href="../css/alertify.core.css" rel="stylesheet" type="text/css">
    <link href="../css/alertify.default.css" rel="stylesheet" type="text/css">

    <!-- Tinytable -->
    <link rel="stylesheet" href="../library/css/tinytable/style-navigation.css" />
	<script type="text/javascript" src="../library/js/tinytable/script.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<!-- Notifikasi keberhasilan login -->
	<?php
		error_reporting(0);
		parse_str($_SERVER['QUERY_STRING'], $query_string);

		if (!empty($license)) 
		{
			if ($query_string['id'] && $query_string['user'] && !$query_string['ref'])	echo '<script>alertify.success("Login Berhasil !")</script>';
		}
	?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php 
                	$user = "user=".$query_string['user'];
					$id = "id=".$query_string['id'];
					$ref = "ref=".date("YmdHi");

            		echo '<a class="navbar-brand" href="index.php?'.$id.'&'.$user.'&'.$ref.'">Halaman Administrator</a>';
            	?>
                
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $query_string['user']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                        	<?php 
                    			echo '<a href="library/logout.php?'.$id.'&'.$user.'&'.$ref.'"><i class="fa fa-fw fa-sign-out"></i> Log Out</a>';
                    		?>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <?php
                        if (!$query_string["page"]) echo '<li class="active">';
                        else echo '<li>';
                        echo '<a href="index.php?'.$id.'&'.$user.'&'.$ref.'"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>';
                        echo '</li>';

                        if ($query_string["page"] == "pengguna" || $query_string["page"] == "detail") echo '<li class="active">';
                        else echo '<li>';
                        echo '<a href="index.php?'.$id.'&'.$user.'&'.$ref.'&page=pengguna"><i class="fa fa-fw fa-table"></i> Report Pengguna SPK</a>';
                        echo "</li>";

                        if ($query_string["page"] == "daftar_pertanyaan" || $query_string["page"] == "update_pertanyaan") echo '<li class="active">';
                        else echo '<li>';
                        echo '<a href="index.php?'.$id.'&'.$user.'&'.$ref.'&page=daftar_pertanyaan"><i class="fa fa-fw fa-table"></i> Daftar Pertanyaan</a>';
                        echo "</li>";

                    ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Administrator</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i><a href="../index.html"> Home</a>
                            </li>
                            <li>
                                <i class="fa fa-user"></i>
                                <?php echo '<a href="index.php?'.$id.'&'.$user.'&'.$ref.'" > Admin</a>'; ?>
                            </li>
                            <li class="active">
                            <?php
                            	$page = $query_string["page"];
                            	switch ($page) 
                            	{
                            		case 'pengguna':
                            		{
                            			echo '<i class="fa fa-dashboard"></i> Report Pengguna SPK';
                            		}break;

                            		case 'daftar_pertanyaan':
                            		{
                            			echo '<i class="fa fa-table"></i> Daftar Pertanyaan';
                            		}break;

                                    case 'update_pertanyaan' :
                                    {
                                        echo '<i class="fa fa-table"></i><a href="index.php?'.$id.'&'.$user.'&'.$ref.'&page=daftar_pertanyaan'.'" > Daftar Pertanyaan </a></li>';
                                        echo '<li class="active"><i class="fa fa-user"></i> Update Pertanyaan </li>';
                                    }break;

                            		case 'detail':
                            		{
                            			echo '<i class="fa fa-dashboard"></i><a href="index.php?'.$id.'&'.$user.'&'.$ref.'&page=pengguna'.'" > Report Pengguna SPK </a></li>';
                            			echo '<li class="active"><i class="fa fa-user"></i> User Detail </li>';
                            		}break;

                            		default:
                            		{
                            			echo '<i class="fa fa-dashboard"></i> Dashboard';
                            		}break;
                            	}
                            ?>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <!-- Flot Charts -->
                <div class="row">
                    <div class="col-lg-12">
                    	<h2 class="page-header">SMILE ! All is Well ! All is Well ...</h2>
                    	<div>
                    		<?php include ('library/page.php'); ?>
                    	</div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                       	<div style="height:50px;"></div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- JQuery Load -->
    <script src="../js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script src="../js/plugins/morris/morris-data.js"></script>

    <!-- Flot Charts JavaScript -->
    <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
    <script src="../js/plugins/flot/jquery.flot.js"></script>
    <script src="../js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="../js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="../js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="../js/plugins/flot/flot-data.js"></script>

</body>

</html>

<?php
	}
?>