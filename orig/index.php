<?php 
session_start();
include ('library/koneksi.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description"  content="" />
		<meta name="keywords" content="" />
		<meta name="robots" content="ALL,FOLLOW" />
		<meta name="Author" content="AIT" />
		<meta http-equiv="imagetoolbar" content="no" />
		<title>SPK Benih Padi</title>
		<link rel="stylesheet" href="library/css/reset.css" type="text/css" />
		<link rel="stylesheet" href="library/css/screen.css" type="text/css" />
		<link rel="stylesheet" href="library/css/fancybox.css" type="text/css" />
		<link rel="stylesheet" href="library/css/jquery.wysiwyg.css" type="text/css" />
		<link rel="stylesheet" href="library/css/jquery.ui.css" type="text/css" />
		<link rel="stylesheet" href="library/css/visualize.css" type="text/css" />
		<link rel="stylesheet" href="library/css/visualize-light.css" type="text/css" />
	</head>
<body>
		<div class="pagetop">
			<div class="head pagesize">
				<div class="head_top">
				 
					<div class="logo clear"> 
					  <h1 align="center"><a href="index.php" title="View dashboard"><a href="index.php" title="SPK Topsis"><a href="index.php" title="View dashboard"></a></h1>
					  <form id="form1" method="post" action="">
					    <p align="center">
					      <input type="image" name="logo" id="logo" src="library/images/logopadinew.jpg" />
					    </p>
				      </form>
					  <h1 align="center"><span class="header">SPK TOPSIS</span></h1>
					  <p align="center"><span class="h2">Pemilihan jenis benih padi</span></p>
					  <div align="center">
					    <blockquote>&nbsp;</blockquote>
					  </div>
					  <blockquote>
					    <h2 align="center" class="h2">&nbsp;</h2>
				      </blockquote>
<p>&nbsp;</p>
				  </div>
				</div>
				<div class="menu">
					<ul class="clear">
                  <li class="first"><a href="index.php"  title="">Home</a></li>
						<li><a href="index.php?id=profil">Profil</a></li>						
						<li><a href="index.php?id=start_spk">SPK Test</a></li>	
						<li><a href="index.php?id=hasil">Hasil penilaian SPK</a></li>
						<li><a href="index.php?id=bukutamu">Keterangan </a></li>
						<li><a href="admin/login.php">Login </a></li>
						<?php  if (!empty ($_SESSION['nama'])) { ?><li><div style="width:150px;"><a href="index.php?id=logout">Selesai dan Logout</marquee></a></div></li>
						<?php  }?>
					</ul>
				</div>
			</div>
		</div>
		<div class="breadcrumb">
			<div class="bread-links pagesize">
				<ul class="clear">
				  <li></li>
				</ul>
			</div>
		</div>
		<div class="main pagesize">
			<div class="main-wrap">
				<div class="page clear">
					<div class="content-box">
						<div class="box-body">
							<div class="box-header clear">
								<h3>Selamat Datang</h3>
							</div>
							<div class="box-wrap clear">
								<?php  include ('file/page.php'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
			<div class="pagesize clear">
				<p class="bt-space15"><span class="copy"><strong>� <?php  echo date('Y');?> Copyright by Suwoto, Teknik Informatika ITATS. </strong></span> Powered by <a href="admin/index.php">SPK TOPSIS ADMIN.</a></p>
			</div>
		</div>
		 
	</body>
</html>
