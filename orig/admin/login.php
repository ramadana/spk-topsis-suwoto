<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description"  content="" />
		<meta name="keywords" content="" />
		<meta name="robots" content="ALL,FOLLOW" />
		<meta name="Author" content="AIT" />
		<meta http-equiv="imagetoolbar" content="no" />
		<title>TERMINATOR: Dashboard</title>
		<link rel="stylesheet" href="../library/css/screen.css" type="text/css" />
	</head>
	<body class="login">
		<div class="login-box">
			<div class="login-border">
				<div class="login-style">
					<div class="login-header">
						<div class="logo clear">
							<img src="file:///C|/xampp/htdocs/spk_topsis/library/images/ico_users_64.png" alt="" class="picture" />
							<span class="textlogo">
								<span class="title">SPK TOPSIS</span>
								<span class="text">Login Administrator</span>
							</span>
						</div>
					</div>
					<form method="post" action="library/auth_login.php">
						<div class="login-inside">
							<div class="login-data">
								<div class="row clear">
									<label for="user">Username:</label>
				    				<input type="text" name="username" size="25" class="text" id="user" />
				    				</div>
				 				<div class="row clear">
									<label for="password">Password:</label>
									<input type="password" name="password" size="25" class="text" id="password" />
								</div>
								<input type="submit" name="submit" class="button" value="Login" />
							</div>
						</div>
						<div class="login-footer clear">
							<a href="../" class="button green fr-space">Back to Page</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="login-links">
			<p><strong>&copy; 2014 Copyright by <a href="#">suwoto Information Technology.</a></strong> All rights reserved.</p>
		</div>
		</body>
</html>
