<?php 
include ("../library/koneksi.php");
session_start();
if (isset ($_SESSION['id']))
$license=$_SESSION['id'];
if (empty ($license)){
		echo "<script>document.location.href='../../login.php?status=failed';</script>\n";
}else{

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description"  content="" />
		<meta name="keywords" content="" />
		<meta name="robots" content="ALL,FOLLOW" />
		<meta name="Author" content="AIT" />
		<meta http-equiv="imagetoolbar" content="no" />
		<title>SPK jenis benih padi </title>
		<link rel="stylesheet" href="../library/css/reset.css" type="text/css" />
		<link rel="stylesheet" href="../library/css/screen.css" type="text/css" />
		<link rel="stylesheet" href="../library/css/tinytable/style.css" />
		<script type="text/javascript" src="../library/js/tinytable/script.js"></script>

	</head>
	<body>
		<div class="pagetop">
			<div class="head pagesize">
			  <div class="head_top">
                <div class="topbuts">
                  <ul class="clear">
                    <li><a href="library/logout.php" class="red">Logout</a></li>
                  </ul>
                  <div class="user clear"><span class="user-detail"> <span class="name">Welcome, <?php  echo $_SESSION['username']; ?></span> </span> </div>
                </div>
			    <div class="logo clear"> 
			      <div align="center">
			        <form id="form1" method="post" action="">
			          <p>
			            <input type="image" name="logo" id="logo" src="../library/images/logopadinew.jpg" />
		              </p>
			          <h1>SPK TOPSIS</h1>
			          <p>Pemilihan jenis benih padi</p>
<p>&nbsp;</p>
		            </form>
			        <p>&nbsp;</p>
			        <p>&nbsp;</p>
			      </div>
                </div>
		      </div>
			  <div class="menu">
					<ul class="clear">
						<li class="active"><a href="index.php">Dashboard</a></li>
						<li class="active"><a href="index.php?id=pengguna">Report Pengguna SPK</a></li>
						<li class="active"><a href="index.php?id=daftar_pertanyaan">Daftar Pertanyaan</a> </li>
					</ul>
			  </div>
		  </div>
		</div>
		<div class="breadcrumb">
			<div class="bread-links pagesize">
				<ul class="clear">
					
					
				</ul>
			</div>
		</div>
		<div class="main pagesize">
			<div class="main-wrap">
				<div class="page clear">
					<div class="content-box">
						<div class="box-body">
							<div class="box-header clear">
								<h2>SMILE YOU DONT CRY</h2>
							</div>
							<div class="box-wrap clear">
								<?php  include ('library/page.php'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
		  <div class="pagesize clear">
<p class="bt-space15"><span class="copy"><strong>� <?php  echo date('Y');?> Copyright by Suwoto, Teknik Informatika ITATS. </strong></span> Powered by <a href="admin/index.php">SPK TOPSIS ADMIN.</a></p>
		  </div>
		</div>
	</body>
</html>
<?php 
}
?>
