<style>
#box_info{
width:100%;
height:150px;
margin-bottom:55px;
}
#gambar{
float:left;
width:150px;
height:250px;
border-bottom:solid 1px #999999;
margin-right:5px;
}
#keterangan{
float:left;
width:790px;
height:250px;
border-bottom:solid 1px #999999;

}
</style>
<?php  for ($a=1; $a<=1; $a++){?>
<div id="box_info">
	<div id="gambar">
			<?php  
			if ($a==1) { echo "<img src='library/images/pd1.jpg' style='width:150px'>";}
			

			?>	
	</div>
	<div id="keterangan">
		<h4>Benih Padi</h4> 
		<p align="justify">Padi merupakan sumber karbohidrat utama bagi mayoritas penduduk dunia. Dan sebagai bahan pangan utama hampir 90 persen penduduk Indonesia. Sehingga dapat dikatakan bahwa padi merupakan bahan makanan pokok utama. Di Negara Indonesia sendiri padi menjadi sangat dominan, sehingga memiliki kedudukan penting dan telah menjadi komoditas strategis. Ketersediaan beras memegang peranan penting bagi ketahanan pangan. Sehingga penggunaan benih padi bermutu sangat penting, karena dapat mengurangi jumlah pemakaian benih dan meningkatkan pendapatan para pelaku usaha khususnya para petani. Memilih Jenis benih padi merupakan pengambilan keputusan yang cukup rumit karena melibatkan berbagai kriteria, seperti umur tanaman, anakan produktif, ketahanan hama penyakit, tekstur, daya hasil dan lain sebagainya. Sebagai bahan pertimbangan dalam pengambilan keputusan. Dengan adanya perkembangan Varietas benih padi yang berkembang secara luas. Dengan situasi Alam yang tidak menentu dan banyaknya bencana yang tidak terduga membuat para pelaku usaha serta orang perorang ataupun petani sangat membutuhkan kehadiran benih padi yang sesuai dengan kriteria. Karena keberhasilan dalam budidaya pertanian sendiri sangat ditentukan oleh benih yang digunakan. Oleh karena itu, diperlukan seleksi layak dan tepat untuk memiliki sebuah benih padi untuk dimiliki. Untuk mendukung proses pengambilan keputusan tersebut dapat dibuat suatu system berbasis computer yang dikenal sebagai Decision Support System (DSS) atau Sistem pendukung keputusan.</p>
		
	</div>
	
</div>
<?php  }?>


