<style type="text/css">
<!--
.style5 {color: #000000; font-weight: bold; }
.style6 {color: #000000; }
-->
</style>
<p align="center" class="style5">Tabel range untuk setiap alternatif</p>
<div align="justify">
  <table border="1" cellspacing="0" cellpadding="0" width="576">
    <tr>
      <td width="104"><div align="center" class="style6"><br />
        <strong>Ranking Kecocokan</strong> </div></td>
      <td width="472" colspan="5"><p align="center" class="style5">Kritea</p></td>
    </tr>
    <tr>
      <td width="104" valign="top"><p class="style6"><strong>&nbsp;</strong></p></td>
      <td width="85"><p align="center" class="style6"><strong>K1</strong></p></td>
      <td width="95"><p align="center" class="style6"><strong>K2</strong></p></td>
      <td width="113"><p align="center" class="style6"><strong>K3</strong></p></td>
      <td width="85"><p align="center" class="style6"><strong>K4</strong></p></td>
      <td width="94"><p align="center" class="style6"><strong>K5</strong></p></td>
    </tr>
    <tr>
      <td width="104" valign="top"><p align="center" class="style5">Kurang    (1)</p></td>
      <td width="85"><p align="center"><strong>&ge;    125 hari</strong></p></td>
      <td width="95"><p align="center"><strong>&le;    13 batang</strong></p></td>
      <td width="113"><p align="left"><strong>Rentan hama wereng coklat  biotip 1,2,3. Wereng hijau,rentan penyakit bakteri hawar daun strain 3,4.  rentan virus tungro</strong></p></td>
      <td width="85"><p align="center"><strong>Pera</strong></p></td>
      <td width="94"><p align="center"><strong>&le;    3 ton/ha</strong></p></td>
    </tr>
    <tr>
      <td width="104" valign="top"><p align="center" class="style6"><strong>Sedang    (2)</strong></p></td>
      <td width="85"><p align="center"><strong>&le;    115 hari</strong></p></td>
      <td width="95"><p align="center"><strong>14    -17 batang </strong></p></td>
      <td width="113"><p align="left"><strong> Agak tahan hama wereng  coklat biotip 1,2,3. Wereng hijau, Agak tahan penyakit bakteri hawar daun  strain 3,4. Agak tahan virus tungro</strong></p></td>
      <td width="85"><p align="center"><strong>    Pulen</strong></p></td>
      <td width="94"><p align="center"><strong>3<br />
        s/d<br />
        8    ton/ha</strong></p></td>
    </tr>
    <tr>
      <td width="104" valign="top"><p align="center" class="style6"><strong>Baik&nbsp; &nbsp;&nbsp;&nbsp;(3)</strong></p></td>
      <td width="85"><p align="center"><strong>&le;    105 hari</strong></p></td>
      <td width="95"><p align="center"><strong>&ge;    18 batang</strong></p></td>
      <td width="113"><p align="left"><strong>Tahan hama wereng coklat  biotip 1,2,3. Wereng hijau, Agak Tahan penyakit bakteri hawar daun strain 3,4. tahan  virus tungro</strong></p></td>
      <td width="85"><p align="center"><strong>Pulen    Dan Harum</strong></p></td>
      <td width="94"><p align="center"><strong>&ge;    8 ton/ha</strong></p></td>
    </tr>
  </table>
</div>
<p align="center" class="style6"><strong> Tabel value kriteria pada setiap  alternatif</strong></p>
<div align="justify">
  <table border="1" cellspacing="0" cellpadding="0" width="576">
    <tr>
      <td width="100" valign="top"><p><strong>&nbsp;</strong></p></td>
      <td width="476" colspan="5" valign="top"><p align="center" class="style6"><strong>Kritea</strong></p></td>
    </tr>
    <tr>
      <td width="100" valign="top"><p align="center" class="style6"><strong>Alternative</strong></p></td>
      <td width="80"><p align="center" class="style6"><strong>K1</strong></p></td>
      <td width="104"><p align="center" class="style6"><strong>K2</strong></p></td>
      <td width="151"><p align="center" class="style6"><strong>K3</strong></p></td>
      <td width="66"><p align="center" class="style6"><strong>K4</strong></p></td>
      <td width="76"><p align="center" class="style6"><strong>K5 </strong></p></td>
    </tr>
    <tr>
      <td width="100" valign="top"><p align="center" class="style6"><strong>Situ bagendit </strong></p></td>
      <td width="80"><p align="center"><strong>110    s/d<br />
        120    hari</strong></p></td>
      <td width="104"><p align="center"><strong>12    -13 batang</strong></p></td>
      <td width="151"><p align="center"><strong>Agak    tahan terhadap bakteri hawar daun strain III dan IV</strong></p></td>
      <td width="66"><p align="center"><strong>Pulen</strong></p></td>
      <td width="76"><p align="center"><strong>3<br />
        s/d<br />
        5    ton/ha</strong></p></td>
    </tr>
    <tr>
      <td width="100" valign="top"><p align="center" class="style6"><strong>Ciherang</strong></p></td>
      <td width="80"><p align="center"><strong>116<br />
        s/d<br />
        125    hari</strong></p></td>
      <td width="104"><p align="center"><strong>14    -17 batang</strong></p></td>
      <td width="151"><p align="center"><strong>Tahan    terhadap wereng coklat biotip 2, 3 dan bakteri hawar daun strain III dan IV</strong></p></td>
      <td width="66"><p align="center"><strong>Pulen</strong></p></td>
      <td width="76"><p align="center"><strong>5 <br />
        s/d<br />
        7    ton/ha</strong></p></td>
    </tr>
    <tr>
      <td width="100" valign="top"><p align="center" class="style6"><strong>IR 64 </strong></p>      </td>
      <td width="80"><p align="center"><strong>105    hari</strong></p></td>
      <td width="104"><p align="center"><strong>25    batang</strong></p></td>
      <td width="151"><p align="center"><strong>Tahan    terhadap wereng coklat biotip 1, 2, 3&nbsp;    dan wereng hijau, tahan&nbsp;    bakteri&nbsp; busuk daun, tahan virus    kerdil rumput</strong></p></td>
      <td width="66"><p align="center"><strong>Pulen </strong></p></td>
      <td width="76"><p align="center"><strong>5 <br />
        s/d<br />
        6    ton/ha </strong></p></td>
    </tr>
    <tr>
      <td width="100" valign="top"><p align="center" class="style6"><strong>Cibogo</strong></p></td>
      <td width="80"><p align="center"><strong>115<br />
        s/d<br />
        125    hari</strong></p></td>
      <td width="104"><p align="center"><strong>12    -19 batang</strong></p></td>
      <td width="151"><p align="center"><strong>Tahan    terhadap wereng coklat biotip 2, Agak tahan hawar daun strain IV&nbsp; biotip 3. Rentan terhadap virus tungro</strong></p></td>
      <td width="66"><p align="center"><strong>Pulen</strong></p></td>
      <td width="76"><p align="center"><strong>7<br />
        s/d<br />
        8,1    ton/ha</strong></p></td>
    </tr>
    <tr>
      <td width="100" valign="top"><p align="center" class="style6"><strong> Way apo buru </strong></p></td>
      <td width="80"><p align="center"><strong>155    hari</strong></p></td>
      <td width="104"><p align="center"><strong>15    &ndash; 18 batang&nbsp; </strong></p></td>
      <td width="151"><p align="center"><strong>Rentan    terhadap wereng coklat biotip 1, 2, 3. Rentan hawar daun strain IV dan rentan    penyakit tungro</strong></p></td>
      <td width="66"><p align="center"><strong>Pulen</strong></p></td>
      <td width="76"><p align="center"><strong>5 <br />
        s/d<br />
        8    ton/ha</strong></p></td>
    </tr>
  </table>
</div>
