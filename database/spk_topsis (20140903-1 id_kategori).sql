-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2014 at 03:24 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spk_topsis`
--

-- --------------------------------------------------------

--
-- Table structure for table `alternatif`
--

CREATE TABLE IF NOT EXISTS `alternatif` (
  `id_alt` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `alt` float NOT NULL,
  `id_kategori` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_alt`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=338 ;

--
-- Dumping data for table `alternatif`
--

INSERT INTO `alternatif` (`id_alt`, `user`, `alt`, `id_kategori`) VALUES
(226, 'rahman', 0.414213, NULL),
(227, 'rahman', 0.532182, NULL),
(228, 'rahman', 0.999996, NULL),
(229, 'rahman', 0.20387, NULL),
(230, 'huda', 0.370778, NULL),
(231, 'huda', 0.696521, NULL),
(232, 'huda', 0.662827, NULL),
(233, 'huda', 0.541643, NULL),
(234, 'lutfi', 0.305206, NULL),
(235, 'lutfi', 0.725551, NULL),
(236, 'lutfi', 0.250136, NULL),
(237, 'lutfi', 0.475832, NULL),
(238, 'wanto', 0.437737, NULL),
(239, 'wanto', 0.580059, NULL),
(240, 'wanto', 0.562529, NULL),
(241, 'wanto', 0.509651, NULL),
(242, 'beki', 0.318803, NULL),
(243, 'beki', 0.464043, NULL),
(244, 'beki', 0.999998, NULL),
(245, 'beki', 0.460218, NULL),
(246, 'latif', 0.00000000715296, NULL),
(247, 'latif', 0.999999, NULL),
(248, 'latif', 0.360063, NULL),
(249, 'latif', 0.246763, NULL),
(250, 'hari', 0.34858, NULL),
(251, 'hari', 0.999998, NULL),
(252, 'hari', 0.374815, NULL),
(253, 'hari', 0.438858, NULL),
(254, 'saipul', 0.379315, NULL),
(255, 'saipul', 0.582773, NULL),
(256, 'saipul', 0.999998, NULL),
(257, 'saipul', 0.000000865971, NULL),
(258, 'arie', 0.626184, NULL),
(259, 'arie', 0.291585, NULL),
(260, 'arie', 0.401893, NULL),
(261, 'arie', 0.999998, NULL),
(262, 'bari', 0.733411, NULL),
(263, 'bari', 0.266587, NULL),
(264, 'bari', 0.659524, NULL),
(265, 'bari', 0.546604, NULL),
(266, 'yono', 0.281132, NULL),
(267, 'yono', 0.663528, NULL),
(268, 'yono', 0.718867, NULL),
(269, 'yono', 0.336469, NULL),
(270, 'oko', 0.478329, NULL),
(271, 'oko', 0.521671, NULL),
(272, 'oko', 0.4199, NULL),
(273, 'oko', 0.811811, NULL),
(274, 'arip', 0.7223, NULL),
(275, 'arip', 0.309912, NULL),
(276, 'arip', 0.277697, NULL),
(277, 'arip', 0.706953, NULL),
(278, 'priyambodo', 0.440423, NULL),
(279, 'priyambodo', 0.3466, NULL),
(280, 'priyambodo', 0.653399, NULL),
(281, 'priyambodo', 0.364925, NULL),
(282, 'fajar', 0.999998, NULL),
(283, 'fajar', 0.431459, NULL),
(284, 'fajar', 0.254531, NULL),
(285, 'fajar', 0.435652, NULL),
(286, 'darmawan', 0.543662, NULL),
(287, 'darmawan', 0.543662, NULL),
(288, 'darmawan', 0.00000143813, NULL),
(289, 'darmawan', 0.999999, NULL),
(290, 'agus', 0.588398, NULL),
(291, 'agus', 0.446564, NULL),
(292, 'agus', 0.384986, NULL),
(293, 'agus', 0.615012, NULL),
(294, 'hendri', 0.766515, NULL),
(295, 'hendri', 0.618856, NULL),
(296, 'hendri', 0.233483, NULL),
(297, 'hendri', 0.381142, NULL),
(298, 'eko', 0.710101, NULL),
(299, 'eko', 0.639117, NULL),
(300, 'eko', 0.289897, NULL),
(301, 'eko', 0.360882, NULL),
(302, 'bejo', 0.441359, NULL),
(303, 'bejo', 0.999998, NULL),
(304, 'bejo', 0.232887, NULL),
(305, 'bejo', 0.585315, NULL),
(306, 'kiro', 0.375515, NULL),
(307, 'kiro', 0.999999, NULL),
(308, 'kiro', 0.56786, NULL),
(309, 'kiro', 0.359871, NULL),
(310, 'badar', 0.00000158059, NULL),
(311, 'badar', 0.546642, NULL),
(312, 'badar', 0.614664, NULL),
(313, 'badar', 0.385336, NULL),
(314, 'Pramandha', 0.38709, NULL),
(315, 'Pramandha', 0.574368, NULL),
(316, 'Pramandha', 0.509462, NULL),
(317, 'Pramandha', 0.846735, NULL),
(318, 'no name', 0.261293, NULL),
(319, 'no name', 0.269113, NULL),
(320, 'no name', 1, NULL),
(321, 'no name', 0.449659, NULL),
(322, 'ren-san', 0.19334, 1),
(323, 'ren-san', 0.548348, 1),
(324, 'ren-san', 0.669456, 1),
(325, 'ren-san', 0.379561, 1),
(326, 'Tohru', 0.575922, 1),
(327, 'Tohru', 0.491406, 1),
(328, 'Tohru', 0.219976, 1),
(329, 'Tohru', 0.999997, 1),
(330, 'Kissuo', 0.367154, 1),
(331, 'Kissuo', 0.374838, 2),
(332, 'Kissuo', 0.412442, 3),
(333, 'Kissuo', 0.999998, 4),
(334, 'kardus', 0.603519, 1),
(335, 'kardus', 0.227475, 2),
(336, 'kardus', 0.999998, 3),
(337, 'kardus', 0.00000000772071, 4);

-- --------------------------------------------------------

--
-- Table structure for table `jarak`
--

CREATE TABLE IF NOT EXISTS `jarak` (
  `id_jarak` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `d1` float NOT NULL,
  `d2` float NOT NULL,
  `d3` float NOT NULL,
  `d4` float NOT NULL,
  `status_jarak` varchar(3) NOT NULL,
  PRIMARY KEY (`id_jarak`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

--
-- Dumping data for table `jarak`
--

INSERT INTO `jarak` (`id_jarak`, `user`, `d1`, `d2`, `d3`, `d4`, `status_jarak`) VALUES
(45, 'rahman', 1, 0.808611, 0.00000576016, 1.53172, '+'),
(46, 'rahman', 0.707104, 0.919861, 1.58114, 0.392236, '-'),
(47, 'huda', 1.41202, 0.654652, 0.625539, 1.05871, '+'),
(48, 'huda', 0.83205, 1.50251, 1.22971, 1.25109, '-'),
(49, 'lutfi', 2.04591, 0.774601, 2.11977, 1.81266, '+'),
(50, 'lutfi', 0.898715, 2.04778, 0.707104, 1.64551, '-'),
(51, 'wanto', 1.60699, 1.04039, 0.944089, 1.41202, '+'),
(52, 'wanto', 1.25109, 1.43709, 1.21397, 1.4676, '-'),
(53, 'beki', 1.51089, 1.26114, 0.00000298485, 1.48003, '+'),
(54, 'beki', 0.707104, 1.09193, 1.94494, 1.26187, '-'),
(55, 'latif', 2.71854, 0.00000263801, 1.94692, 2.36442, '+'),
(56, 'latif', 0.0000000194456, 2.71854, 1.09544, 0.774593, '-'),
(57, 'hari', 1.58815, 0.00000504159, 1.76698, 1.51291, '+'),
(58, 'hari', 0.849835, 2.16539, 1.05935, 1.18322, '-'),
(59, 'saipul', 1.54484, 1.09327, 0.00000464774, 2.24955, '+'),
(60, 'saipul', 0.94409, 1.52705, 2.24954, 0.00000194804, '-'),
(61, 'arie', 0.875596, 1.71793, 1.75119, 0.00000409382, '+'),
(62, 'arie', 1.46672, 0.707104, 1.1767, 2.1098, '-'),
(63, 'bari', 0.707111, 1.94532, 0.884646, 1.4676, '+'),
(64, 'bari', 1.94532, 0.707104, 1.71361, 1.76931, '-'),
(65, 'yono', 2.17307, 1.00001, 0.84984, 1.97203, '+'),
(66, 'yono', 0.849835, 1.97202, 2.17307, 0.999996, '-'),
(67, 'oko', 1.7693, 1.62231, 1.49685, 0.516394, '+'),
(68, 'oko', 1.62231, 1.76931, 1.08348, 2.22763, '-'),
(69, 'arip', 0.707111, 1.74416, 1.83921, 0.806716, '+'),
(70, 'arip', 1.8392, 0.783284, 0.707104, 1.94614, '-'),
(71, 'priyambodo', 1.79682, 1.81659, 0.963626, 1.74028, '+'),
(72, 'priyambodo', 1.41421, 0.963621, 1.81659, 0.999996, '-'),
(73, 'fajar', 0.00000470741, 1.45679, 2.07097, 1.64992, '+'),
(74, 'fajar', 2.40601, 1.10554, 0.707104, 1.27367, '-'),
(75, 'darmawan', 1.00001, 1.00001, 2.10223, 0.00000307916, '+'),
(76, 'darmawan', 1.19136, 1.19136, 0.00000302328, 2.10223, '-'),
(77, 'agus', 1.14781, 1.55839, 1.35761, 0.84984, '+'),
(78, 'agus', 1.64083, 1.25745, 0.849835, 1.35761, '-'),
(79, 'hendri', 0.707111, 1.10555, 2.3214, 1.79506, '+'),
(80, 'hendri', 2.3214, 1.79505, 0.707104, 1.10554, '-'),
(81, 'eko', 0.707111, 0.919867, 1.73205, 1.62907, '+'),
(82, 'eko', 1.73205, 1.62906, 0.707104, 0.919861, '-'),
(83, 'bejo', 1.19496, 0.00000463938, 1.93796, 1.01815, '+'),
(84, 'bejo', 0.94409, 2.0253, 0.588344, 1.43709, '-'),
(85, 'kiro', 1.17592, 0.00000217353, 0.830953, 1.48003, '+'),
(86, 'kiro', 0.707104, 1.69788, 1.09193, 0.83205, '-'),
(87, 'badar', 1.2325, 0.786795, 0.654652, 1.04426, '+'),
(88, 'badar', 0.00000194808, 0.948686, 1.04426, 0.654652, '-'),
(89, 'Pramandha', 1.53226, 1.31113, 1.39876, 0.392232, '+'),
(90, 'Pramandha', 0.967714, 1.76931, 1.45271, 2.16695, '-'),
(91, 'no name', 2.3523, 2.10372, 0.00000102999, 1.93218, '+'),
(92, 'no name', 0.83205, 0.774593, 2.49512, 1.5787, '-'),
(93, 'rama', 1.54919, 1.81659, 0.948684, 0.774601, '+'),
(94, 'rama', 0.948686, 0.0000000253138, 1.54919, 1.22474, '-'),
(95, 'ren-san', 1.96682, 1.08091, 0.94281, 1.81659, '+'),
(96, 'ren-san', 0.471406, 1.31234, 1.90949, 1.11132, '-'),
(97, 'Tohru', 1.0549, 1.38305, 2.08624, 0.00000578349, '+'),
(98, 'Tohru', 1.43261, 1.33631, 0.588344, 2.16761, '-'),
(99, 'Kissuo', 1.71614, 2.08659, 1.99129, 0.0000043041, '+'),
(100, 'Kissuo', 0.995643, 1.25109, 1.3978, 2.43291, '-'),
(101, 'kardus', 1.16905, 2.40139, 0.00000422008, 2.69568, '+'),
(102, 'kardus', 1.77951, 0.707104, 2.69567, 0.0000000208125, '-');

-- --------------------------------------------------------

--
-- Table structure for table `matrix_r`
--

CREATE TABLE IF NOT EXISTS `matrix_r` (
  `id_matrix_r` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `r1` float NOT NULL,
  `r2` float NOT NULL,
  `r3` float NOT NULL,
  `r4` float NOT NULL,
  PRIMARY KEY (`id_matrix_r`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=254 ;

--
-- Dumping data for table `matrix_r`
--

INSERT INTO `matrix_r` (`id_matrix_r`, `user`, `r1`, `r2`, `r3`, `r4`) VALUES
(138, 'rahman', 0.471405, 0.392232, 0.392232, 0.5),
(139, 'rahman', 0.471405, 0.392232, 0.588348, 0.5),
(140, 'rahman', 0.707107, 0.588348, 0.588348, 0.5),
(141, 'rahman', 0.235702, 0.588348, 0.392232, 0.5),
(142, 'huda', 0.436436, 0.5, 0.5547, 0.208514),
(143, 'huda', 0.436436, 0.5, 0.5547, 0.625543),
(144, 'huda', 0.654654, 0.5, 0.5547, 0.417029),
(145, 'huda', 0.436436, 0.5, 0.27735, 0.625543),
(146, 'lutfi', 0.258199, 0.5547, 0.471405, 0.377964),
(147, 'lutfi', 0.516398, 0.5547, 0.707107, 0.755929),
(148, 'lutfi', 0.258199, 0.27735, 0.471405, 0.377964),
(149, 'lutfi', 0.774597, 0.5547, 0.235702, 0.377964),
(150, 'wanto', 0.436436, 0.392232, 0.625543, 0.235702),
(151, 'wanto', 0.436436, 0.392232, 0.625543, 0.471405),
(152, 'wanto', 0.654654, 0.588348, 0.417029, 0.471405),
(153, 'wanto', 0.436436, 0.588348, 0.208514, 0.707107),
(154, 'beki', 0.27735, 0.436436, 0.471405, 0.316228),
(155, 'beki', 0.5547, 0.436436, 0.471405, 0.316228),
(156, 'beki', 0.5547, 0.654654, 0.707107, 0.632456),
(157, 'beki', 0.5547, 0.436436, 0.235702, 0.632456),
(158, 'latif', 0.258199, 0.436436, 0.258199, 0.258199),
(159, 'latif', 0.774597, 0.654654, 0.774597, 0.774597),
(160, 'latif', 0.516398, 0.436436, 0.258199, 0.516398),
(161, 'latif', 0.258199, 0.436436, 0.516398, 0.258199),
(162, 'hari', 0.471405, 0.471405, 0.316228, 0.316228),
(163, 'hari', 0.707107, 0.707107, 0.632456, 0.632456),
(164, 'hari', 0.235702, 0.471405, 0.632456, 0.316228),
(165, 'hari', 0.471405, 0.235702, 0.316228, 0.632456),
(166, 'saipul', 0.471405, 0.258199, 0.417029, 0.436436),
(167, 'saipul', 0.471405, 0.516398, 0.625543, 0.436436),
(168, 'saipul', 0.707107, 0.774597, 0.625543, 0.654654),
(169, 'saipul', 0.235702, 0.258199, 0.208514, 0.436436),
(170, 'arie', 0.5547, 0.516398, 0.471405, 0.5547),
(171, 'arie', 0.27735, 0.258199, 0.471405, 0.27735),
(172, 'arie', 0.5547, 0.258199, 0.235702, 0.5547),
(173, 'arie', 0.5547, 0.774597, 0.707107, 0.5547),
(174, 'bari', 0.625543, 0.588348, 0.625543, 0.471405),
(175, 'bari', 0.208514, 0.392232, 0.208514, 0.471405),
(176, 'bari', 0.417029, 0.588348, 0.417029, 0.707107),
(177, 'bari', 0.625543, 0.392232, 0.625543, 0.235702),
(178, 'yono', 0.235702, 0.471405, 0.235702, 0.471405),
(179, 'yono', 0.471405, 0.707107, 0.471405, 0.707107),
(180, 'yono', 0.707107, 0.471405, 0.707107, 0.471405),
(181, 'yono', 0.471405, 0.235702, 0.471405, 0.235702),
(182, 'oko', 0.208514, 0.774597, 0.208514, 0.625543),
(183, 'oko', 0.625543, 0.258199, 0.625543, 0.208514),
(184, 'oko', 0.417029, 0.258199, 0.417029, 0.417029),
(185, 'oko', 0.625543, 0.516398, 0.625543, 0.625543),
(186, 'arip', 0.625543, 0.707107, 0.471405, 0.654654),
(187, 'arip', 0.417029, 0.471405, 0.235702, 0.436436),
(188, 'arip', 0.208514, 0.235702, 0.471405, 0.436436),
(189, 'arip', 0.625543, 0.471405, 0.707107, 0.436436),
(190, 'priyambodo', 0.436436, 0.316228, 0.707107, 0.258199),
(191, 'priyambodo', 0.654654, 0.316228, 0.471405, 0.258199),
(192, 'priyambodo', 0.436436, 0.632456, 0.471405, 0.774597),
(193, 'priyambodo', 0.436436, 0.632456, 0.235702, 0.516398),
(194, 'fajar', 0.707107, 0.707107, 0.707107, 0.632456),
(195, 'fajar', 0.471405, 0.471405, 0.471405, 0.316228),
(196, 'fajar', 0.471405, 0.235702, 0.235702, 0.316228),
(197, 'fajar', 0.235702, 0.471405, 0.471405, 0.632456),
(198, 'darmawan', 0.471405, 0.538816, 0.471405, 0.538816),
(199, 'darmawan', 0.471405, 0.538816, 0.471405, 0.538816),
(200, 'darmawan', 0.235702, 0.359211, 0.235702, 0.359211),
(201, 'darmawan', 0.707107, 0.538816, 0.707107, 0.538816),
(202, 'agus', 0.436436, 0.235702, 0.707107, 0.5547),
(203, 'agus', 0.436436, 0.707107, 0.235702, 0.5547),
(204, 'agus', 0.436436, 0.471405, 0.471405, 0.27735),
(205, 'agus', 0.654654, 0.471405, 0.471405, 0.5547),
(206, 'hendri', 0.707107, 0.707107, 0.471405, 0.707107),
(207, 'hendri', 0.471405, 0.471405, 0.707107, 0.471405),
(208, 'hendri', 0.235702, 0.235702, 0.471405, 0.235702),
(209, 'hendri', 0.471405, 0.471405, 0.235702, 0.471405),
(210, 'eko', 0.707107, 0.588348, 0.471405, 0.588348),
(211, 'eko', 0.471405, 0.588348, 0.707107, 0.392232),
(212, 'eko', 0.471405, 0.392232, 0.235702, 0.392232),
(213, 'eko', 0.235702, 0.392232, 0.471405, 0.588348),
(214, 'bejo', 0.471405, 0.436436, 0.417029, 0.392232),
(215, 'bejo', 0.707107, 0.654654, 0.625543, 0.588348),
(216, 'bejo', 0.235702, 0.436436, 0.208514, 0.588348),
(217, 'bejo', 0.471405, 0.436436, 0.625543, 0.392232),
(218, 'kiro', 0.5, 0.436436, 0.471405, 0.27735),
(219, 'kiro', 0.5, 0.654654, 0.707107, 0.5547),
(220, 'kiro', 0.5, 0.436436, 0.471405, 0.5547),
(221, 'kiro', 0.5, 0.436436, 0.235702, 0.5547),
(222, 'badar', 0.5, 0.436436, 0.316228, 0.436436),
(223, 'badar', 0.5, 0.436436, 0.632456, 0.436436),
(224, 'badar', 0.5, 0.654654, 0.632456, 0.436436),
(225, 'badar', 0.5, 0.436436, 0.316228, 0.654654),
(226, 'Pramandha', 0.417029, 0.588348, 0.208514, 0.417029),
(227, 'Pramandha', 0.208514, 0.392232, 0.625543, 0.625543),
(228, 'Pramandha', 0.625543, 0.588348, 0.417029, 0.208514),
(229, 'Pramandha', 0.625543, 0.392232, 0.625543, 0.625543),
(230, 'no name', 0.258199, 0.288675, 0.5547, 0.223607),
(231, 'no name', 0.516398, 0.288675, 0.27735, 0.223607),
(232, 'no name', 0.774597, 0.866025, 0.5547, 0.67082),
(233, 'no name', 0.258199, 0.288675, 0.5547, 0.67082),
(234, 'rama', 0.632456, 0.5, 0.5, 0.258199),
(235, 'rama', 0.316228, 0.5, 0.5, 0.258199),
(236, 'rama', 0.316228, 0.5, 0.5, 0.774597),
(237, 'rama', 0.632456, 0.5, 0.5, 0.516398),
(238, 'ren-san', 0.316228, 0.471405, 0.392232, 0.258199),
(239, 'ren-san', 0.632456, 0.471405, 0.392232, 0.516398),
(240, 'ren-san', 0.632456, 0.235702, 0.588348, 0.774597),
(241, 'ren-san', 0.316228, 0.707107, 0.588348, 0.258199),
(242, 'Tohru', 0.392232, 0.516398, 0.471405, 0.566947),
(243, 'Tohru', 0.392232, 0.258199, 0.471405, 0.566947),
(244, 'Tohru', 0.588348, 0.258199, 0.235702, 0.188982),
(245, 'Tohru', 0.588348, 0.774597, 0.707107, 0.566947),
(246, 'Kissuo', 0.417029, 0.392232, 0.223607, 0.516398),
(247, 'Kissuo', 0.625543, 0.392232, 0.223607, 0.258199),
(248, 'Kissuo', 0.208514, 0.588348, 0.67082, 0.258199),
(249, 'Kissuo', 0.625543, 0.588348, 0.67082, 0.774597),
(250, 'kardus', 0.471405, 0.516398, 0.67082, 0.516398),
(251, 'kardus', 0.471405, 0.258199, 0.223607, 0.258199),
(252, 'kardus', 0.707107, 0.774597, 0.67082, 0.774597),
(253, 'kardus', 0.235702, 0.258199, 0.223607, 0.258199);

-- --------------------------------------------------------

--
-- Table structure for table `matrix_y`
--

CREATE TABLE IF NOT EXISTS `matrix_y` (
  `id_matrix_y` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `y1` float NOT NULL,
  `y2` float NOT NULL,
  `y3` float NOT NULL,
  `y4` float NOT NULL,
  PRIMARY KEY (`id_matrix_y`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=282 ;

--
-- Dumping data for table `matrix_y`
--

INSERT INTO `matrix_y` (`id_matrix_y`, `user`, `y1`, `y2`, `y3`, `y4`) VALUES
(166, 'rahman', 1.41421, 0.784464, 1.1767, 1.5),
(167, 'rahman', 1.41421, 0.784464, 1.76504, 1.5),
(168, 'rahman', 2.12132, 1.1767, 1.76504, 1.5),
(169, 'rahman', 0.707106, 1.1767, 1.1767, 1.5),
(170, 'huda', 1.30931, 1, 1.6641, 0.625542),
(171, 'huda', 1.30931, 1, 1.6641, 1.87663),
(172, 'huda', 1.96396, 1, 1.6641, 1.25109),
(173, 'huda', 1.30931, 1, 0.83205, 1.87663),
(174, 'lutfi', 0.774597, 1.1094, 1.41421, 1.13389),
(175, 'lutfi', 1.54919, 1.1094, 2.12132, 2.26779),
(176, 'lutfi', 0.774597, 0.5547, 1.41421, 1.13389),
(177, 'lutfi', 2.32379, 1.1094, 0.707106, 1.13389),
(178, 'wanto', 1.30931, 0.784464, 1.87663, 0.707106),
(179, 'wanto', 1.30931, 0.784464, 1.87663, 1.41421),
(180, 'wanto', 1.96396, 1.1767, 1.25109, 1.41421),
(181, 'wanto', 1.30931, 1.1767, 0.625542, 2.12132),
(182, 'beki', 0.83205, 0.872872, 1.41421, 0.948684),
(183, 'beki', 1.6641, 0.872872, 1.41421, 0.948684),
(184, 'beki', 1.6641, 1.30931, 2.12132, 1.89737),
(185, 'beki', 1.6641, 0.872872, 0.707106, 1.89737),
(186, 'latif', 0.774597, 0.872872, 0.774597, 0.774597),
(187, 'latif', 2.32379, 1.30931, 2.32379, 2.32379),
(188, 'latif', 1.54919, 0.872872, 0.774597, 1.54919),
(189, 'latif', 0.774597, 0.872872, 1.54919, 0.774597),
(190, 'hari', 1.41421, 0.94281, 0.948684, 0.948684),
(191, 'hari', 2.12132, 1.41421, 1.89737, 1.89737),
(192, 'hari', 0.707106, 0.94281, 1.89737, 0.948684),
(193, 'hari', 1.41421, 0.471404, 0.948684, 1.89737),
(194, 'saipul', 1.41421, 0.516398, 1.25109, 1.30931),
(195, 'saipul', 1.41421, 1.0328, 1.87663, 1.30931),
(196, 'saipul', 2.12132, 1.54919, 1.87663, 1.96396),
(197, 'saipul', 0.707106, 0.516398, 0.625542, 1.30931),
(198, 'arie', 1.6641, 1.0328, 1.41421, 1.6641),
(199, 'arie', 0.83205, 0.516398, 1.41421, 0.83205),
(200, 'arie', 1.6641, 0.516398, 0.707106, 1.6641),
(201, 'arie', 1.6641, 1.54919, 2.12132, 1.6641),
(202, 'bari', 1.87663, 1.1767, 1.87663, 1.41421),
(203, 'bari', 0.625542, 0.784464, 0.625542, 1.41421),
(204, 'bari', 1.25109, 1.1767, 1.25109, 2.12132),
(205, 'bari', 1.87663, 0.784464, 1.87663, 0.707106),
(206, 'yono', 0.707106, 0.94281, 0.707106, 1.41421),
(207, 'yono', 1.41421, 1.41421, 1.41421, 2.12132),
(208, 'yono', 2.12132, 0.94281, 2.12132, 1.41421),
(209, 'yono', 1.41421, 0.471404, 1.41421, 0.707106),
(210, 'oko', 0.625542, 1.54919, 0.625542, 1.87663),
(211, 'oko', 1.87663, 0.516398, 1.87663, 0.625542),
(212, 'oko', 1.25109, 0.516398, 1.25109, 1.25109),
(213, 'oko', 1.87663, 1.0328, 1.87663, 1.87663),
(214, 'arip', 1.87663, 1.41421, 1.41421, 1.96396),
(215, 'arip', 1.25109, 0.94281, 0.707106, 1.30931),
(216, 'arip', 0.625542, 0.471404, 1.41421, 1.30931),
(217, 'arip', 1.87663, 0.94281, 2.12132, 1.30931),
(218, 'priyambodo', 1.30931, 0.632456, 2.12132, 0.774597),
(219, 'priyambodo', 1.96396, 0.632456, 1.41421, 0.774597),
(220, 'priyambodo', 1.30931, 1.26491, 1.41421, 2.32379),
(221, 'priyambodo', 1.30931, 1.26491, 0.707106, 1.54919),
(222, 'fajar', 2.12132, 1.41421, 2.12132, 1.89737),
(223, 'fajar', 1.41421, 0.94281, 1.41421, 0.948684),
(224, 'fajar', 1.41421, 0.471404, 0.707106, 0.948684),
(225, 'fajar', 0.707106, 0.94281, 1.41421, 1.89737),
(226, 'darmawan', 1.41421, 1.07763, 1.41421, 1.61645),
(227, 'darmawan', 1.41421, 1.07763, 1.41421, 1.61645),
(228, 'darmawan', 0.707106, 0.718422, 0.707106, 1.07763),
(229, 'darmawan', 2.12132, 1.07763, 2.12132, 1.61645),
(230, 'agus', 1.30931, 0.471404, 2.12132, 1.6641),
(231, 'agus', 1.30931, 1.41421, 0.707106, 1.6641),
(232, 'agus', 1.30931, 0.94281, 1.41421, 0.83205),
(233, 'agus', 1.96396, 0.94281, 1.41421, 1.6641),
(234, 'hendri', 2.12132, 1.41421, 1.41421, 2.12132),
(235, 'hendri', 1.41421, 0.94281, 2.12132, 1.41421),
(236, 'hendri', 0.707106, 0.471404, 1.41421, 0.707106),
(237, 'hendri', 1.41421, 0.94281, 0.707106, 1.41421),
(238, 'eko', 2.12132, 1.1767, 1.41421, 1.76504),
(239, 'eko', 1.41421, 1.1767, 2.12132, 1.1767),
(240, 'eko', 1.41421, 0.784464, 0.707106, 1.1767),
(241, 'eko', 0.707106, 0.784464, 1.41421, 1.76504),
(242, 'bejo', 1.41421, 0.872872, 1.25109, 1.1767),
(243, 'bejo', 2.12132, 1.30931, 1.87663, 1.76504),
(244, 'bejo', 0.707106, 0.872872, 0.625542, 1.76504),
(245, 'bejo', 1.41421, 0.872872, 1.87663, 1.1767),
(246, 'kiro', 1.5, 0.872872, 1.41421, 0.83205),
(247, 'kiro', 1.5, 1.30931, 2.12132, 1.6641),
(248, 'kiro', 1.5, 0.872872, 1.41421, 1.6641),
(249, 'kiro', 1.5, 0.872872, 0.707106, 1.6641),
(250, 'badar', 1.5, 0.872872, 0.948684, 1.30931),
(251, 'badar', 1.5, 0.872872, 1.89737, 1.30931),
(252, 'badar', 1.5, 1.30931, 1.89737, 1.30931),
(253, 'badar', 1.5, 0.872872, 0.948684, 1.96396),
(254, 'Pramandha', 1.25109, 1.1767, 0.625542, 1.25109),
(255, 'Pramandha', 0.625542, 0.784464, 1.87663, 1.87663),
(256, 'Pramandha', 1.87663, 1.1767, 1.25109, 0.625542),
(257, 'Pramandha', 1.87663, 0.784464, 1.87663, 1.87663),
(258, 'no name', 0.774597, 0.57735, 1.6641, 0.670821),
(259, 'no name', 1.54919, 0.57735, 0.83205, 0.670821),
(260, 'no name', 2.32379, 1.73205, 1.6641, 2.01246),
(261, 'no name', 0.774597, 0.57735, 1.6641, 2.01246),
(262, 'rama', 1.89737, 1, 1.5, 0.774597),
(263, 'rama', 0.948684, 1, 1.5, 0.774597),
(264, 'rama', 0.948684, 1, 1.5, 2.32379),
(265, 'rama', 1.89737, 1, 1.5, 1.54919),
(266, 'ren-san', 0.948684, 0.94281, 1.1767, 0.774597),
(267, 'ren-san', 1.89737, 0.94281, 1.1767, 1.54919),
(268, 'ren-san', 1.89737, 0.471404, 1.76504, 2.32379),
(269, 'ren-san', 0.948684, 1.41421, 1.76504, 0.774597),
(270, 'Tohru', 1.1767, 1.0328, 1.41421, 1.70084),
(271, 'Tohru', 1.1767, 0.516398, 1.41421, 1.70084),
(272, 'Tohru', 1.76504, 0.516398, 0.707106, 0.566946),
(273, 'Tohru', 1.76504, 1.54919, 2.12132, 1.70084),
(274, 'Kissuo', 1.25109, 0.784464, 0.670821, 1.54919),
(275, 'Kissuo', 1.87663, 0.784464, 0.670821, 0.774597),
(276, 'Kissuo', 0.625542, 1.1767, 2.01246, 0.774597),
(277, 'Kissuo', 1.87663, 1.1767, 2.01246, 2.32379),
(278, 'kardus', 1.41421, 1.0328, 2.01246, 1.54919),
(279, 'kardus', 1.41421, 0.516398, 0.670821, 0.774597),
(280, 'kardus', 2.12132, 1.54919, 2.01246, 2.32379),
(281, 'kardus', 0.707106, 0.516398, 0.670821, 0.774597);

-- --------------------------------------------------------

--
-- Table structure for table `solusi`
--

CREATE TABLE IF NOT EXISTS `solusi` (
  `id_solusi` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `a1` float NOT NULL,
  `a2` float NOT NULL,
  `a3` float NOT NULL,
  `a4` float NOT NULL,
  `status_solusi` varchar(3) NOT NULL,
  PRIMARY KEY (`id_solusi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=121 ;

--
-- Dumping data for table `solusi`
--

INSERT INTO `solusi` (`id_solusi`, `user`, `a1`, `a2`, `a3`, `a4`, `status_solusi`) VALUES
(63, 'rahman', 2.12132, 1.1767, 1.76504, 1.5, '+'),
(64, 'rahman', 0.707106, 0.784464, 1.1767, 1.5, '-'),
(65, 'huda', 1.96396, 1, 1.6641, 1.87663, '+'),
(66, 'huda', 1.30931, 1, 0.83205, 0.625542, '-'),
(67, 'lutfi', 2.32379, 1.1094, 2.12132, 2.26779, '+'),
(68, 'lutfi', 0.774597, 0.5547, 0.707106, 1.13389, '-'),
(69, 'wanto', 1.96396, 1.1767, 1.87663, 2.12132, '+'),
(70, 'wanto', 1.30931, 0.784464, 0.625542, 0.707106, '-'),
(71, 'beki', 1.6641, 1.30931, 2.12132, 1.89737, '+'),
(72, 'beki', 0.83205, 0.872872, 0.707106, 0.948684, '-'),
(73, 'latif', 2.32379, 1.30931, 2.32379, 2.32379, '+'),
(74, 'latif', 0.774597, 0.872872, 0.774597, 0.774597, '-'),
(75, 'hari', 2.12132, 1.41421, 1.89737, 1.89737, '+'),
(76, 'hari', 0.707106, 0.471404, 0.948684, 0.948684, '-'),
(77, 'saipul', 2.12132, 1.54919, 1.87663, 1.96396, '+'),
(78, 'saipul', 0.707106, 0.516398, 0.625542, 1.30931, '-'),
(79, 'arie', 1.6641, 1.54919, 2.12132, 1.6641, '+'),
(80, 'arie', 0.83205, 0.516398, 0.707106, 0.83205, '-'),
(81, 'bari', 1.87663, 1.1767, 1.87663, 2.12132, '+'),
(82, 'bari', 0.625542, 0.784464, 0.625542, 0.707106, '-'),
(83, 'yono', 2.12132, 1.41421, 2.12132, 2.12132, '+'),
(84, 'yono', 0.707106, 0.471404, 0.707106, 0.707106, '-'),
(85, 'oko', 1.87663, 1.54919, 1.87663, 1.87663, '+'),
(86, 'oko', 0.625542, 0.516398, 0.625542, 0.625542, '-'),
(87, 'arip', 1.87663, 1.41421, 2.12132, 1.96396, '+'),
(88, 'arip', 0.625542, 0.471404, 0.707106, 1.30931, '-'),
(89, 'priyambodo', 1.96396, 1.26491, 2.12132, 2.32379, '+'),
(90, 'priyambodo', 1.30931, 0.632456, 0.707106, 0.774597, '-'),
(91, 'fajar', 2.12132, 1.41421, 2.12132, 1.89737, '+'),
(92, 'fajar', 0.707106, 0.471404, 0.707106, 0.948684, '-'),
(93, 'darmawan', 2.12132, 1.07763, 2.12132, 1.61645, '+'),
(94, 'darmawan', 0.707106, 0.718422, 0.707106, 1.07763, '-'),
(95, 'agus', 1.96396, 1.41421, 2.12132, 1.6641, '+'),
(96, 'agus', 1.30931, 0.471404, 0.707106, 0.83205, '-'),
(97, 'hendri', 2.12132, 1.41421, 2.12132, 2.12132, '+'),
(98, 'hendri', 0.707106, 0.471404, 0.707106, 0.707106, '-'),
(99, 'eko', 2.12132, 1.1767, 2.12132, 1.76504, '+'),
(100, 'eko', 0.707106, 0.784464, 0.707106, 1.1767, '-'),
(101, 'bejo', 2.12132, 1.30931, 1.87663, 1.76504, '+'),
(102, 'bejo', 0.707106, 0.872872, 0.625542, 1.1767, '-'),
(103, 'kiro', 1.5, 1.30931, 2.12132, 1.6641, '+'),
(104, 'kiro', 1.5, 0.872872, 0.707106, 0.83205, '-'),
(105, 'badar', 1.5, 1.30931, 1.89737, 1.96396, '+'),
(106, 'badar', 1.5, 0.872872, 0.948684, 1.30931, '-'),
(107, 'Pramandha', 1.87663, 1.1767, 1.87663, 1.87663, '+'),
(108, 'Pramandha', 0.625542, 0.784464, 0.625542, 0.625542, '-'),
(109, 'no name', 2.32379, 1.73205, 1.6641, 2.01246, '+'),
(110, 'no name', 0.774597, 0.57735, 0.83205, 0.670821, '-'),
(111, 'rama', 1.89737, 1, 1.5, 2.32379, '+'),
(112, 'rama', 0.948684, 1, 1.5, 0.774597, '-'),
(113, 'ren-san', 1.89737, 1.41421, 1.76504, 2.32379, '+'),
(114, 'ren-san', 0.948684, 0.471404, 1.1767, 0.774597, '-'),
(115, 'Tohru', 1.76504, 1.54919, 2.12132, 1.70084, '+'),
(116, 'Tohru', 1.1767, 0.516398, 0.707106, 0.566946, '-'),
(117, 'Kissuo', 1.87663, 1.1767, 2.01246, 2.32379, '+'),
(118, 'Kissuo', 0.625542, 0.784464, 0.670821, 0.774597, '-'),
(119, 'kardus', 2.12132, 1.54919, 2.01246, 2.32379, '+'),
(120, 'kardus', 0.707106, 0.516398, 0.670821, 0.774597, '-');

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE IF NOT EXISTS `jawaban` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `kategori` int(8) NOT NULL,
  `user` varchar(100) NOT NULL,
  `jawaban_k1` text NOT NULL,
  `jawaban_k2` text NOT NULL,
  `jawaban_k3` text NOT NULL,
  `jawaban_k4` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kategori` (`kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=246 ;

--
-- Dumping data for table `jawaban`
--

INSERT INTO `jawaban` (`id`, `kategori`, `user`, `jawaban_k1`, `jawaban_k2`, `jawaban_k3`, `jawaban_k4`) VALUES
(125, 1, 'rahman', '2', '2', '2', '2'),
(126, 2, 'rahman', '2', '2', '3', '2'),
(127, 3, 'rahman', '3', '3', '3', '2'),
(128, 4, 'rahman', '1', '3', '2', '2'),
(129, 1, 'huda', '2', '2', '2', '1'),
(130, 2, 'huda', '2', '2', '2', '3'),
(131, 3, 'huda', '3', '2', '2', '2'),
(132, 4, 'huda', '2', '2', '1', '3'),
(133, 1, 'lutfi', '1', '2', '2', '1'),
(134, 2, 'lutfi', '2', '2', '3', '2'),
(135, 3, 'lutfi', '1', '1', '2', '1'),
(136, 4, 'lutfi', '3', '2', '1', '1'),
(137, 1, 'wanto', '2', '2', '3', '1'),
(138, 2, 'wanto', '2', '2', '3', '2'),
(139, 3, 'wanto', '3', '3', '2', '2'),
(140, 4, 'wanto', '2', '3', '1', '3'),
(141, 1, 'beki', '1', '2', '2', '1'),
(142, 2, 'beki', '2', '2', '2', '1'),
(143, 3, 'beki', '2', '3', '3', '2'),
(144, 4, 'beki', '2', '2', '1', '2'),
(145, 1, 'latif', '1', '2', '1', '1'),
(146, 2, 'latif', '3', '3', '3', '3'),
(147, 3, 'latif', '2', '2', '1', '2'),
(148, 4, 'latif', '1', '2', '2', '1'),
(149, 1, 'hari', '2', '2', '1', '1'),
(150, 2, 'hari', '3', '3', '2', '2'),
(151, 3, 'hari', '1', '2', '2', '1'),
(152, 4, 'hari', '2', '1', '1', '2'),
(153, 1, 'saipul', '2', '1', '2', '2'),
(154, 2, 'saipul', '2', '2', '3', '2'),
(155, 3, 'saipul', '3', '3', '3', '3'),
(156, 4, 'saipul', '1', '1', '1', '2'),
(157, 1, 'arie', '2', '2', '2', '2'),
(158, 2, 'arie', '1', '1', '2', '1'),
(159, 3, 'arie', '2', '1', '1', '2'),
(160, 4, 'arie', '2', '3', '3', '2'),
(161, 1, 'bari', '3', '3', '3', '2'),
(162, 2, 'bari', '1', '2', '1', '2'),
(163, 3, 'bari', '2', '3', '2', '3'),
(164, 4, 'bari', '3', '2', '3', '1'),
(165, 1, 'yono', '1', '2', '1', '2'),
(166, 2, 'yono', '2', '3', '2', '3'),
(167, 3, 'yono', '3', '2', '3', '2'),
(168, 4, 'yono', '2', '1', '2', '1'),
(169, 1, 'oko', '1', '3', '1', '3'),
(170, 2, 'oko', '3', '1', '3', '1'),
(171, 3, 'oko', '2', '1', '2', '2'),
(172, 4, 'oko', '3', '2', '3', '3'),
(173, 1, 'arip', '3', '3', '2', '3'),
(174, 2, 'arip', '2', '2', '1', '2'),
(175, 3, 'arip', '1', '1', '2', '2'),
(176, 4, 'arip', '3', '2', '3', '2'),
(177, 1, 'priyambodo', '2', '1', '3', '1'),
(178, 2, 'priyambodo', '3', '1', '2', '1'),
(179, 3, 'priyambodo', '2', '2', '2', '3'),
(180, 4, 'priyambodo', '2', '2', '1', '2'),
(181, 1, 'fajar', '3', '3', '3', '2'),
(182, 2, 'fajar', '2', '2', '2', '1'),
(183, 3, 'fajar', '2', '1', '1', '1'),
(184, 4, 'fajar', '1', '2', '2', '2'),
(185, 1, 'darmawan', '2', '3', '2', '3'),
(186, 2, 'darmawan', '2', '3', '2', '3'),
(187, 3, 'darmawan', '1', '2', '1', '2'),
(188, 4, 'darmawan', '3', '3', '3', '3'),
(189, 1, 'agus', '2', '1', '3', '2'),
(190, 2, 'agus', '2', '3', '1', '2'),
(191, 3, 'agus', '2', '2', '2', '1'),
(192, 4, 'agus', '3', '2', '2', '2'),
(193, 1, 'hendri', '3', '3', '2', '3'),
(194, 2, 'hendri', '2', '2', '3', '2'),
(195, 3, 'hendri', '1', '1', '2', '1'),
(196, 4, 'hendri', '2', '2', '1', '2'),
(197, 1, 'eko', '3', '3', '2', '3'),
(198, 2, 'eko', '2', '3', '3', '2'),
(199, 3, 'eko', '2', '2', '1', '2'),
(200, 4, 'eko', '1', '2', '2', '3'),
(201, 1, 'bejo', '2', '2', '2', '2'),
(202, 2, 'bejo', '3', '3', '3', '3'),
(203, 3, 'bejo', '1', '2', '1', '3'),
(204, 4, 'bejo', '2', '2', '3', '2'),
(205, 1, 'asdasd', '', '', '', ''),
(206, 1, 'kiro', '2', '2', '2', '1'),
(207, 2, 'kiro', '2', '3', '3', '2'),
(208, 3, 'kiro', '2', '2', '2', '2'),
(209, 4, 'kiro', '2', '2', '1', '2'),
(210, 1, 'badar', '2', '2', '1', '2'),
(211, 2, 'badar', '2', '2', '2', '2'),
(212, 3, 'badar', '2', '3', '2', '2'),
(213, 4, 'badar', '2', '2', '1', '3'),
(214, 1, 'Pramandha', '2', '3', '1', '2'),
(215, 2, 'Pramandha', '1', '2', '3', '3'),
(216, 3, 'Pramandha', '3', '3', '2', '1'),
(217, 4, 'Pramandha', '3', '2', '3', '3'),
(218, 1, 'no name', '1', '1', '2', '1'),
(219, 2, 'no name', '2', '1', '1', '1'),
(220, 3, 'no name', '3', '3', '2', '3'),
(221, 4, 'no name', '1', '1', '2', '3'),
(222, 1, 'yuina', '1', '1', '2', '2'),
(223, 2, 'yuina', '1', '1', '1', '1'),
(224, 3, 'yuina', '3', '3', '3', '2'),
(225, 4, 'yuina', '2', '1', '2', '2'),
(226, 1, 'rama', '2', '2', '3', '1'),
(227, 2, 'rama', '1', '2', '3', '1'),
(228, 3, 'rama', '1', '2', '3', '3'),
(229, 4, 'rama', '2', '2', '3', '2'),
(230, 1, 'ren-san', '1', '2', '2', '1'),
(231, 2, 'ren-san', '2', '2', '2', '2'),
(232, 3, 'ren-san', '2', '1', '3', '3'),
(233, 4, 'ren-san', '1', '3', '3', '1'),
(234, 1, 'Tohru', '2', '2', '2', '3'),
(235, 2, 'Tohru', '2', '1', '2', '3'),
(236, 3, 'Tohru', '3', '1', '1', '1'),
(237, 4, 'Tohru', '3', '3', '3', '3'),
(238, 1, 'Kissuo', '2', '2', '1', '2'),
(239, 2, 'Kissuo', '3', '2', '1', '1'),
(240, 3, 'Kissuo', '1', '3', '3', '1'),
(241, 4, 'Kissuo', '3', '3', '3', '3'),
(242, 1, 'kardus', '2', '2', '3', '2'),
(243, 2, 'kardus', '2', '1', '1', '1'),
(244, 3, 'kardus', '3', '3', '3', '3'),
(245, 4, 'kardus', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `spk_kategori`
--

CREATE TABLE IF NOT EXISTS `spk_kategori` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `spk_kategori`
--

INSERT INTO `spk_kategori` (`id`, `kategori`) VALUES
(1, 'SITU BAGENDIT'),
(2, 'CIHERANG'),
(3, 'IR 64'),
(4, 'CIBOGO');

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE IF NOT EXISTS `pertanyaan` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `kategori` int(8) NOT NULL,
  `k1` text NOT NULL,
  `k2` text NOT NULL,
  `k3` text NOT NULL,
  `k4` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kategori` (`kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `pertanyaan`
--

INSERT INTO `pertanyaan` (`id`, `kategori`, `k1`, `k2`, `k3`, `k4`) VALUES
(11, 1, 'Bagaimana menurut pendapat anda, jika umur tanaman yang diperlukan Situ Bagendit adalah 110 - 120  hari??', 'Jika anakan produktif yang ditawarkan Situ Bagendit sebesar  12 - 13 Apa pendapat anda?? ', 'Situ Bagendit memiliki ketahanan hama penyakit seperti agak tahan terhadap bakteri hawar daun strain III IV. Bagaimana pendapat anda??', 'Bagaimana penilaian anda, jika daya hasil yang ditawarkan oleh Situ Bagendit adalah\r\n3 - 5 ton/ha ??'),
(12, 2, 'Bagaimana menurut pendapat anda, jika umur tanaman yang diperlukan Ciherang adalah 118 - 125  hari??', '\r\nJika anakan produktif yang ditawarkan Ciherang  sebesar  14 - 17 batang. Apa pendapat anda?? ', '\r\nCiherang  memiliki ketahanan hama penyakit seperti Tahan terhadap wereng coklat biotip 2, 3 dan bakteri hawar daun strain III IV. Bagaimana pendapat anda??', 'Bagaimana penilaian anda, jika daya hasil yang ditawarkan oleh Ciherang adalah\r\n5 - 7 ton/ha ??'),
(13, 3, 'Bagaimana menurut pendapat anda, jika umur tanaman yang diperlukan IR 64 adalah 105  hari??', 'Jika anakan produktif yang ditawarkan IR 64 sebesar  25 Apa pendapat anda?? ', 'IR 64 memiliki ketahanan hama penyakit seperti tahan terhadap wereng coklat biotip 2,3 dan wereng hijau, tahan bakteri busuk daun,tahan virus kerdil kerdil rumput. Bagaimana pendapat anda??\r\n', 'Bagaimana penilaian anda, jika daya hasil yang ditawarkan oleh IR 64 adalah\r\n5 - 6 ton/ha ??'),
(15, 4, 'Bagaimana menurut pendapat anda, jika umur tanaman yang diperlukan cibogo adalah 115 - 125  hari??\r\n', 'Jika anakan produktif yang ditawarkan cibogo sebesar  12 - 19 Apa pendapat anda?? \r\n', 'cibogo memiliki ketahanan hama penyakit seperti  tahan terhadap wereng biotip 2, agak tahan bakteri hawar daun strain IV. Rentan terhadap virus tungro. Bagaimana pendapat anda??\r\n', 'Bagaimana penilaian anda, jika daya hasil yang ditawarkan oleh cibogo adalah\r\n7 - 8,1 ton/ha ??');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `username` varchar(35) NOT NULL,
  `password` varchar(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `status`) VALUES
(1, 'suwoto', 'suwoto', 'suwoto', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD CONSTRAINT `jawaban_ibfk_1` FOREIGN KEY (`kategori`) REFERENCES `spk_kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
