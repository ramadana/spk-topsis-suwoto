<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SPK Benih Padi | Suwoto Ito</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Custom Alert Dialog -->
    <script src="js/alertify.js"></script>
    <link href="css/alertify.core.css" rel="stylesheet" type="text/css">
    <link href="css/alertify.default.css" rel="stylesheet" type="text/css"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php 
        session_start();
        include('library/koneksi.php');

        // Surpress error reporting
        error_reporting(0);

        // Parse Query String
        parse_str($_SERVER["QUERY_STRING"], $query_string);
        $status_id = $query_string['id'];
    ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">SPK Benih Padi</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               
                <?php 
                    if ($status_id != "login") {
                        echo '<li class="dropdown">';
                    }else {
                        echo '<li class="dropdown open">';
                    }
                ?>
                
                    <a href="index.php?id=login"><i class="fa fa-sign-in"></i> Sign In </a>
                <!--
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    
                    </ul>
                -->
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <?php
                        $page_id = $query_string["id"];

                        // Home Navigation Selection
                        if (!$page_id) echo '<li class="active">';
                        else echo '<li>';
                        echo '<a href="index.php"><i class="fa fa-fw fa-desktop"></i> Home</a>';
                        echo '</li>';

                        // Profil Navigation Selection
                        if ($page_id == "profil") echo '<li class="active">';
                        else echo '<li>';
                        echo '<a href="index.php?id=profil"><i class="fa fa-fw fa-bar-chart-o"></i> Profil</a>';
                        echo '</li>';

                        // SPK Test Navigation Selection
                        $spk_array = array("start_spk", "spk", "spk2", "spk3", "spk4");
                        if (in_array($page_id, $spk_array)) echo '<li class="active">';
                        else echo '<li>';
                        echo '<a href="index.php?id=start_spk"><i class="fa fa-fw fa-table"></i> SPK Test</a>';
                        echo '</li>';

                        // Hasil Penilain SPK Navigation Selection
                        if ($page_id == "hasil") echo '<li class="active">';
                        else echo '<li>';
                        echo '<a href="index.php?id=hasil"><i class="fa fa-fw fa-edit"></i> Hasil Penilaian SPK</a>';
                        echo '</li>';

                        // Keterangan Navigation Selection
                        if ($page_id == "bukutamu") echo '<li class="active">';
                        else echo '<li>';
                        echo '<a href="index.php?id=bukutamu"><i class="fa fa-fw fa-desktop"></i> Keterangan</a>';
                        echo '</li>';

                    ?>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php 
                                if ($page_id == "login"){
                                    echo '<img src="orig/library/images/ico_users_64.png" alt="" class="picture" /> Login <small>Administrator</small>';
                                }else {
                                    echo "SPK Topsis <small>Pemilihan Jenis Benih Padi</small>";
                                }
                            ?>
                            
                        </h1>
                        <ol class="breadcrumb">
                            <?php
                                switch ($page_id) 
                                {
                                    case 'profil':
                                    {
                                        echo '<li><i class="fa fa-desktop"></i><a href="index.php"> Home </a></li>';
                                        echo '<li class="active"><i class="fa fa-bar-chart-o"></i> Profil</li>';
                                    }break;

                                    case 'start_spk':
                                    {
                                        echo '<li><i class="fa fa-desktop"></i><a href="index.php"> Home </a></li>';
                                        echo '<li class="active"><i class="fa fa-table"></i> SPK Test </li>';
                                    }break;

                                    case 'spk':
                                    {
                                        echo '<li><i class="fa fa-desktop"></i><a href="index.php"> Home </a></li>';
                                        echo '<li><i class="fa fa-table"></i><a href="index.php?id=start_spk"> SPK Test </a></li>';
                                        echo '<li class="active"><i class="fa fa-table"></i> SPK Pertanyaan 1 </li>';
                                    }break;

                                    case 'spk2':
                                    {
                                        echo '<li><i class="fa fa-desktop"></i><a href="index.php"> Home </a></li>';
                                        echo '<li><i class="fa fa-table"></i><a href="index.php?id=start_spk"> SPK Test </a></li>';
                                        echo '<li class="active"><i class="fa fa-table"></i> SPK Pertanyaan 2 </li>';
                                    }break;

                                    case 'spk3':
                                    {
                                        echo '<li><i class="fa fa-desktop"></i><a href="index.php"> Home </a></li>';
                                        echo '<li><i class="fa fa-table"></i><a href="index.php?id=start_spk"> SPK Test </a></li>';
                                        echo '<li class="active"><i class="fa fa-table"></i> SPK Pertanyaan 3 </li>';
                                    }break;

                                    case 'spk4':
                                    {
                                        echo '<li><i class="fa fa-desktop"></i><a href="index.php"> Home </a></li>';
                                        echo '<li><i class="fa fa-table"></i><a href="index.php?id=start_spk"> SPK Test </a></li>';
                                        echo '<li class="active"><i class="fa fa-table"></i> SPK Pertanyaan 4 </li>';
                                    }break;

                                    case 'hasil' :
                                    {
                                        echo '<li><i class="fa fa-desktop"></i><a href="index.php"> Home </a></li>';
                                        echo '<li class="active"><i class="fa fa-user"></i> Hasil Penilaian SPK </li>';
                                    }break;

                                    case 'bukutamu':
                                    {
                                        echo '<li><i class="fa fa-desktop"></i><a href="index.php"> Home </a></li>';
                                        echo '<li class="active"><i class="fa fa-user"></i> Keterangan </li>';
                                    }break;

                                    case 'login':
                                    {
                                        echo '<li><i class="fa fa-desktop"></i><a href="index.php"> Home </a></li>';
                                        echo '<li class="active"><i class="fa fa-user"></i> Login </li>';
                                    }break;

                                    default:
                                    {
                                        echo '<li class="active">';
                                        echo '<i class="fa fa-desktop"></i> Home';
                                        echo '</li>';
                                    }break;
                                }
                            ?>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <!-- Ucapan Pembukaan -->
                <?php 
                    if (!$status_id) {
                        echo '<div class="row">';
                            echo '<div class="col-lg-12">';
                                echo '<p class="lead">Selamat Datang</p>';
                            echo '</div>';
                        echo '</div>';
                    }
                ?>
                <!-- /.row -->

                <!-- Content (Please be advised, that this content still doesn't have rows. 
                     Please implement it on child page) -->
                <?php  include ('file/page.php'); ?>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
