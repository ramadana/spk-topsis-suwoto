<script>

    function login_click() 
    {
        var username = document.getElementById("user").value;
        var password = document.getElementById("password").value;
        if (username.length == 0 && password.length == 0) alertify.alert("Field Username dan Password masih kosong.<br>Mohon isi terlebih dahulu.");
        else if (username.length == 0) alertify.alert("Masukkan username anda.");
        else if (password.length == 0) alertify.alert("Masukkan password anda.");
        else return true;
        return false;
    }

</script>

<div class="row">
    <div class="col-lg-12">
        <p class="lead">Please insert your username and password.</p>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-4">
        <?php 
            error_reporting(0);
            $status = $_GET['status']; 
            if ($status) 
            {
                switch ($status)
                {
                    case 'failed'  : 
                    {
                        echo '<script>alertify.error("Login Gagal !")</script>';
                        echo "<p style='color:#FF0000;font-size:10px;'><i>Maaf kombinasi username dan password yang anda masukkan salah.</i></p>";

                    }break;  

                    case 'success' :
                    {
                        echo '<script>alertify.success("Logout Berhasil !")</script>';
                    }break;
                }
            }
            
        ?>

        <form method="post" action="    admin/library/auth_login.php">
            <div class="form-group">
                <label>Username</label>
                <input class="form-control" name="username" id="user" placeholder="Enter Username">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="form-control" name="password" id="password" placeholder="Enter Password">
            </div>

            <button type="submit" class="btn btn btn-primary" onclick="return login_click();">Login</button>
            <a href="index.php"><button type="button" class="btn btn-link">Back to Home</button></a>
        </form>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-4">
        <div style="height:70px;"></div>
    </div>
</div>
<!-- /.row -->